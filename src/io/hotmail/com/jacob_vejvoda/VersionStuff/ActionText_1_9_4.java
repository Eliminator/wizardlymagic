package io.hotmail.com.jacob_vejvoda.VersionStuff;

import net.minecraft.server.v1_9_R2.ChatComponentText;
import net.minecraft.server.v1_9_R2.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ActionText_1_9_4
{
  public static void sendActionText(Player player, String message)
  {
    PacketPlayOutChat packet = new PacketPlayOutChat(new ChatComponentText(message), (byte)2);
    ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
  }
}
