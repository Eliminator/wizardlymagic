package io.hotmail.com.jacob_vejvoda.VersionStuff;

import net.minecraft.server.v1_11_R1.ChatComponentText;
import net.minecraft.server.v1_11_R1.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ActionText_1_11
{
  public static void sendActionText(Player player, String message)
  {
    PacketPlayOutChat packet = new PacketPlayOutChat(new ChatComponentText(message), (byte)2);
    ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
  }
}
