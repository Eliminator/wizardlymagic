package io.hotmail.com.jacob_vejvoda.WizardlyMagic;

import com.griefcraft.lwc.LWCPlugin;
import com.griefcraft.model.Protection;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.massivecore.ps.PS;
import com.palmergames.bukkit.towny.object.TownyPermission.ActionType;
import com.palmergames.bukkit.towny.utils.PlayerCacheUtil;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import io.hotmail.com.jacob_vejvoda.VersionStuff.ActionText_1_8_6;
import io.hotmail.com.jacob_vejvoda.VersionStuff.ActionText_1_9_4;
import io.hotmail.com.jacob_vejvoda.VersionStuff.ActionText_1_10;
import io.hotmail.com.jacob_vejvoda.VersionStuff.ActionText_1_11;
import io.hotmail.com.jacob_vejvoda.VersionStuff.ActionText_1_12;
import io.hotmail.com.jacob_vejvoda.VersionStuff.ParticleEffects_1_8_6;
import io.hotmail.com.jacob_vejvoda.VersionStuff.ParticleEffects_1_9_4;
import io.hotmail.com.jacob_vejvoda.VersionStuff.ParticleEffects_1_10;
import io.hotmail.com.jacob_vejvoda.VersionStuff.ParticleEffects_1_11;
import io.hotmail.com.jacob_vejvoda.VersionStuff.ParticleEffects_1_12;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

import javax.imageio.ImageIO;
import net.milkbowl.vault.item.Items;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.banner.Pattern;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.WitherSkull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;
import org.yi.acru.bukkit.Lockette.Lockette;

@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
public class WizardlyMagic extends JavaPlugin implements Listener{
	File saveYML = new File(getDataFolder(), "save.yml");
	YamlConfiguration saveFile = YamlConfiguration.loadConfiguration(saveYML);
	HashMap<String, String> recipeMap = new HashMap<String, String>();
	HashMap<String, Inventory> invMap = new HashMap<String, Inventory>();
	ArrayList<UUID> enderList = new ArrayList<UUID>();
	ArrayList<UUID> projList = new ArrayList<UUID>();
	ArrayList<UUID> eggList = new ArrayList<UUID>();
	ArrayList<String> castCool = new ArrayList<String>();
	HashMap<UUID, String> coolMap = new HashMap<UUID, String>();
	HashMap<String, Block> altarMap = new HashMap<String, Block>();
	HashMap<String, Integer> playerMana = new HashMap<String, Integer>();
	HashMap<String, Boolean> showMana = new HashMap<String, Boolean>();
	HashMap<String, Integer> spellBooks = new HashMap<String, Integer>();
	HashMap<String, Integer> staffs = new HashMap<String, Integer>();
	ArrayList<Short> renderedMaps = new ArrayList<Short>();
  
  @Override
  public void onEnable(){
  	//System.out.println("WM enable");
  	getServer().getPluginManager().registerEvents(this, this); 
	//Folder 
	File dir = new File(this.getDataFolder().getParentFile().getPath()+File.separator+this.getName());
	if(!dir.exists())
		dir.mkdir();
  	if (!new File(getDataFolder(), "config.yml").exists()) {
  		//saveDefaultConfig();
  		this.getLogger().log(Level.INFO, "No config.yml found, generating...");
  		//Generate Config
  		boolean generatedConfig = false;
  		for(String version : Arrays.asList("1.12","1.11","1.10","1.9","1.8"))
  			if(Bukkit.getVersion().contains(version)){
  	  	        InputStream in = getClass().getResourceAsStream(version.replace(".", "_") + "_config.yml");
  	  	        isSave(in, "config.yml");
  	  	        this.getLogger().log(Level.INFO, "Config successfully generated!");
  	  	        generatedConfig = true;
  	  	        break;
  			}
  		if(!generatedConfig){
  			this.getLogger().log(Level.SEVERE, "No config available, " + Bukkit.getVersion() + " is not supported!");
  			Bukkit.getPluginManager().disablePlugin(this);
  		}
  	}
//  	if (!new File(getDataFolder(), "spell.png").exists()) {
//  		//File folder = new File(getDataFolder() + File.separator + "spell.png");
//		try {
//	  		BufferedImage i = ImageIO.read(getClass().getResource("spell.png"));
//			ImageIO.write(i, "png", new File(getDataFolder(), "spell.png"));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//  	}
  	//Register Saves
  	if (!saveYML.exists()) {
  		try {
  			saveYML.createNewFile();
  		} catch (IOException e) {
  			e.printStackTrace();
  		}
  	}
  	//Metrics
	try {
	    Metrics metrics = new Metrics(this);
	    metrics.start();
	} catch (IOException e) {
	    // Failed to submit the stats :-(
	}
	addRecipes();
	timer();
  }
  
  public void isSave(InputStream inputStream, String dFile) {

	OutputStream outputStream = null;

	try {
		// write the inputStream to a FileOutputStream
	    File plugins = Bukkit.getServer().getPluginManager().getPlugin(this.getName()).getDataFolder().getParentFile();
		outputStream = new FileOutputStream(new File(plugins.getPath() + File.separator + this.getName() + File.separator + dFile));

		int read = 0;
		byte[] bytes = new byte[1024];

		while ((read = inputStream.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}

		System.out.println("Done!");

	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (outputStream != null) {
			try {
				// outputStream.flush();
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
  }
  
  private void timer(){
	  try{
		  HashMap<String, Integer> pm = (HashMap<String, Integer>)this.playerMana.clone();
		  for (Map.Entry<String, Integer> i : pm.entrySet()){
			  String uuid = getUUID((String)i.getKey());
			  if (uuid != null){
				  int max = getMaxMana(uuid);
				  if (((Integer)i.getValue()).intValue() + 1 <= max) {
					  this.playerMana.put((String)i.getKey(), Integer.valueOf(((Integer)i.getValue()).intValue() + getManaRegen((String)i.getKey())));
				  }
			  }
		  }
	  }catch (Exception x){x.printStackTrace();}
	  //for(Player p : getServer().getOnlinePlayers())
	  //	updateManaBar(p);
	  Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
		  public void run(){
			  timer();
		  }
	  }, 100L);
  }
  
  private void updateManaBar(Player p){
	  if ((!showMana.containsKey(p.getName())) || showMana.get(p.getName()) == false)
		  if(getConfig().getBoolean("manaBar.enable")){
			  String s = getConfig().getString("manaBar.text");
			  int max = getConfig().getInt("startingMana");
			  if (this.saveFile.getString("players." + p.getUniqueId().toString() + ".mana") != null)
			      max = this.saveFile.getInt("players." + p.getUniqueId().toString() + ".mana");
			  int mana = max;
			  if (this.playerMana.containsKey(p.getName()))
			      mana = ((Integer)this.playerMana.get(p.getName())).intValue();
			  //Bar
			  float time = (float) max-mana;
			  float maxTime = (float) max;
	      	  float setTime = (time * 100.0f) / maxTime;
	      	  //setTime = setTime/100.0f;
	      	  //System.out.println("BAR: " + setTime);
	      	  //System.out.println("c: " + mana);
	      	  //System.out.println("m: " + max);
	      	  //setTime = 100 when out of mana
	      	  String bar = getConfig().getString("manaBar.fullColor");
	      	  boolean added = false;
	      	  double at = 100;
	      	  for(int i = 0; i < 30; i++){
	      		  if(added == false && at <= setTime){
	      			  added = true;
	      			  bar = bar + getConfig().getString("manaBar.emptyColor");
	      		  }
	      		  bar = bar + getConfig().getString("manaBar.symbol");
	      		  at = at - 3.333333333;
	      	  }
	      	  showActionBar(p, s.replace("<bar>", bar).replace("<amount>", mana+"/"+max).replace("&", "§"));
		  }else
			  showMana(p);
  }
  
  private void showActionBar(Player p, String s){
	  if(Bukkit.getVersion().contains("1.12")){
		  ActionText_1_12.sendActionText(p, s);
	  }else if(Bukkit.getVersion().contains("1.11")){
		  ActionText_1_11.sendActionText(p, s);
	  }else if(Bukkit.getVersion().contains("1.10")){
		  ActionText_1_10.sendActionText(p, s);
	  }else if(Bukkit.getVersion().contains("1.9.4")){
		  ActionText_1_9_4.sendActionText(p, s);
	  }else if(getServer().getVersion().contains("1.8.6")){
		  ActionText_1_8_6.sendActionText(p, s);
	  }
  }
  
  private int getManaRegen(String name){
    int m = 1;
    
    int wr = 0;
    Player p = getServer().getPlayer(name);
    Color rc = null;
    if (p != null)
    {
      ItemStack[] arrayOfItemStack;
      int j = (arrayOfItemStack = p.getInventory().getArmorContents()).length;
      for (int i = 0; i < j; i++)
      {
        ItemStack s = arrayOfItemStack[i];
        if ((s != null) && (
          (s.getType().equals(Material.LEATHER_BOOTS)) || (s.getType().equals(Material.LEATHER_CHESTPLATE)) || (s.getType().equals(Material.LEATHER_HELMET)) || (s.getType().equals(Material.LEATHER_LEGGINGS))))
        {
          LeatherArmorMeta l = (LeatherArmorMeta)s.getItemMeta();
          Color c = l.getColor();
          if (rc == null) {
            rc = c;
          }
          if (c.equals(rc)) {
            wr++;
          }
        }
      }
    }
    if (wr >= 4) {
      m++;
    }
    int max = getMaxMana(getUUID(name));
    if (m > max) {
      m = max;
    }
    return m;
  }
  
  private int getMaxMana(String uuid){
    int max = getConfig().getInt("startingMana");
    if (this.saveFile.getString("players." + uuid + ".mana") != null) {
      max = this.saveFile.getInt("players." + uuid + ".mana");
    }
    return max;
  }
  
  private String getUUID(String name){
    for (String uuid : this.saveFile.getConfigurationSection("players").getKeys(false)) {
      if ((this.saveFile.getString("players." + uuid + ".name") != null) && (this.saveFile.getString("players." + uuid + ".name").equals(name))) {
        return uuid;
      }
    }
    return null;
  }
  
  @EventHandler(priority=EventPriority.NORMAL)
  public void onPlayerJoin(PlayerJoinEvent e){
    Player p = e.getPlayer();
    this.saveFile.set("players." + p.getUniqueId().toString() + ".name", p.getName());
    try
    {
      this.saveFile.save(this.saveYML);
    }
    catch (IOException localIOException) {}
  }
  
  @EventHandler(priority=EventPriority.NORMAL)
  public void onPlayerPickupItem(PlayerPickupItemEvent e){
    Player p = e.getPlayer();
    try
    {
      Item i = e.getItem();
      ItemStack s = i.getItemStack();
      if (s.getItemMeta().getDisplayName().equals("§7Thrown Grenade")) {
        e.setCancelled(true);
      }
    }
    catch (Exception localException) {}
    if (e.getItem() != null) {
      checkKnowledge(p, e.getItem().getItemStack().getTypeId() + ":" + e.getItem().getItemStack().getDurability(), "item", true);
    }
  }
  
  @EventHandler(priority=EventPriority.NORMAL)
  public void onProjectileHit(ProjectileHitEvent e)
  {
    try
    {
      String projName = null;
      for (MetadataValue v : e.getEntity().getMetadata("projectileMetadata")) {
        projName = v.asString();
      }
      if (projName != null)
      {
        if (((e.getEntity() instanceof EnderPearl)) && 
          ((e.getEntity().getShooter() instanceof Player))) {
          this.enderList.add(((Player)e.getEntity().getShooter()).getUniqueId());
        }
        this.projList.add(e.getEntity().getUniqueId());
        splashEntity(e.getEntity());
      }
    }
    catch (Exception localException) {}
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onPlayerEggThrow(PlayerEggThrowEvent e)
  {
    try
    {
      Egg egg = e.getEgg();
      if (this.eggList.contains(egg.getUniqueId()))
      {
        e.setHatching(false);
        this.eggList.remove(egg.getUniqueId());
      }
    }
    catch (Exception localException) {}
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onPlayerTP(PlayerTeleportEvent e)
  {
    try
    {
      Player p = e.getPlayer();
      if ((e.getCause().equals(PlayerTeleportEvent.TeleportCause.ENDER_PEARL)) && (this.enderList.contains(p.getUniqueId())))
      {
        this.enderList.remove(p.getUniqueId());
        e.setCancelled(true);
      }
    }
    catch (Exception localException) {}
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onPlayerItemConsume(PlayerItemConsumeEvent e){
    try{
      Player p = e.getPlayer();
      if (e.getItem().getItemMeta().getDisplayName().contains("§9§lMana Potion ")){
        int max = getMaxMana(p.getUniqueId().toString());
        int lvl = Integer.parseInt(((String)e.getItem().getItemMeta().getLore().get(1)).replace(" mana", ""));
        int mana = ((Integer)this.playerMana.get(p.getName())).intValue();
        mana += lvl;
        if (mana > max) {
          mana = max;
        }
        this.playerMana.put(p.getName(), Integer.valueOf(mana));
      }
    }
    catch (Exception localException) {}
  }
  
public ItemStack getManaPotion(int lvl){
    String i = "I";
    if (lvl == 2) {
      i = "II";
    } else if (lvl >= 3) {
      i = "III";
    }
    ItemStack p = getItem("373", "§9§lMana Potion " + i, 1, new ArrayList(Arrays.asList(new String[] { "Drink to regen", lvl * 5 + " mana" })));
    return p;
  }
  
  private ItemStack getArcaneScroll()
  {
    return getItem("395", getConfig().getString("arcaneScroll.name").replace("&", "§"), 1, null);
  }
  
  private ItemStack getBookOfKnowledge(Player p){
    ItemStack b = getItem("387", getConfig().getString("bookOfKnowledge.name").replace("&", "§"), 1, null);
    if(p != null)
    	setKnowledgePages(p, b);
    return b;
  }
  
  private ItemStack getBookOfSpells()
  {
    ItemStack b = getItem("387", getConfig().getString("bookOfSpells.name").replace("&", "§"), 1, null);
    setSpellPages(b);
    return b;
  }
  
  private ItemStack getBookOfDrops()
  {
    ItemStack b = getItem("387", getConfig().getString("bookOfDrops.name").replace("&", "§"), 1, null);
    setDropsPages(b);
    return b;
  }
  
  public boolean isArcaneScroll(ItemStack s){
    try{
      if ((s.getTypeId() == 395) && (s.getItemMeta().getDisplayName().equals(getConfig().getString("arcaneScroll.name").replace("&", "§")))) {
        return true;
      }
    }
    catch (Exception localException) {}
    return false;
  }
  
  public boolean isBindingPowder(ItemStack s)
  {
    try
    {
      if (s.getItemMeta().getDisplayName().equals(getConfig().getString("bindingPowderName").replace("&", "§"))) {
        return true;
      }
    }
    catch (Exception localException) {}
    return false;
  }
  
  private ItemStack getStaff(String id)
  {
    int uuid = getNewID("staffs");
    ItemStack staff = getItem("staffs." + id, getConfig());
    ItemMeta m = staff.getItemMeta();
    ArrayList<String> l = (ArrayList)m.getLore();
    if (l == null) {
      l = new ArrayList();
    }
    l.add(0, "§k" + uuid + ":" + id);
    m.setLore(l);
    staff.setItemMeta(m);
    return staff;
  }
  
  private ItemStack getSpell(String spell)
  {
    int m = getConfig().getInt("spells." + spell + ".mana");
    return getItem("358", "§5" + spell + " §5Spell", 1, new ArrayList(Arrays.asList(new String[] { "Mana: " + m })));
  }
  
  private ItemStack getSpellBook()
  {
    return getItem("403", getConfig().getString("spellBook.name").replace("&", "§"), 1, new ArrayList(Arrays.asList(new String[] { "§k" + getNewID("spellBooks") })));
  }
  
  private void setSpellPages(ItemStack book)
  {
    BookMeta m = (BookMeta)book.getItemMeta();
    m.setAuthor(getConfig().getString("bookOfSpells.author"));
    List<String> pages = new ArrayList();
    String aPage = "";
    for (String spell : getConfig().getConfigurationSection("spells").getKeys(false)) {
      if (getConfig().getString("spells." + spell + ".recipe") != null)
      {
        String page = "§0§l" + spell + ":\n";
        if (getConfig().getString("spells." + spell + ".description") != null) {
          page = page + "§8" + getConfig().getString(new StringBuilder("spells.").append(spell).append(".description").toString()) + "\n";
        }
        page = page + "§6/sr " + spell;
        if ((!aPage.contains("\n")) || ((aPage.split("\n").length <= 10) && (aPage.length() < 100)))
        {
          aPage = aPage + " \n" + page;
        }
        else
        {
          pages.add(aPage);
          aPage = page;
        }
      }
    }
    if (!aPage.equals("")) {
      pages.add(aPage);
    }
    m.setPages(pages);
    book.setItemMeta(m);
  }
  
  private void setDropsPages(ItemStack book)
  {
    BookMeta m = (BookMeta)book.getItemMeta();
    m.setAuthor(getConfig().getString("bookOfDrops.author"));
    List<String> pages = new ArrayList();
    String aPage = "";
    for (String mob : getConfig().getConfigurationSection("mobDrops").getKeys(false))
    {
      String page = "§0§l" + mob.substring(0, 1).toUpperCase() + mob.substring(1) + ":\n";
      for (String index : getConfig().getConfigurationSection("mobDrops." + mob).getKeys(false))
      {
        ItemStack s = getItem("mobDrops." + mob + "." + index, getConfig());
        String name = getIdName(s, s.getTypeId() + ":" + s.getDurability()).replace("§f", "§7").replace("§e", "§6");
        page = page + "§8" + name + "§8: " + getConfig().getInt(new StringBuilder("mobDrops.").append(mob).append(".").append(index).append(".chance").toString()) + "%\n";
      }
      if ((!aPage.contains("\n")) || (aPage.split("\n").length < 9))
      {
        aPage = aPage + " \n" + page;
      }
      else
      {
        pages.add(aPage);
        aPage = page;
      }
    }
    if (!aPage.equals("")) {
      pages.add(aPage);
    }
    m.setPages(pages);
    book.setItemMeta(m);
  }
  
  private void setKnowledgePages(Player p, ItemStack book){
    BookMeta m = (BookMeta)book.getItemMeta();
    m.setAuthor(getConfig().getString("bookOfKnowledge.author"));
    List<String> pages = new ArrayList();
    String aPage = "";
    //Iterator localIterator2;
    for(String type : getConfig().getConfigurationSection("knowledge").getKeys(false))
    	for(String id : getConfig().getConfigurationSection("knowledge." + type).getKeys(false)){
    	String name = getConfig().getString("knowledge." + type + "." + id + ".name");
    	String act = "Pickup Item";
    	String kind = "Item";
      if (type.equals("interact")){
        act = "Click Block";
        kind = "Block";
      }
      String c = "§0";
      if (p != null) {
        if (getKnowledge(p).contains(name)) {
          c = "§6";
        } else {
          c = "§c";
        }
      }
      String page = c + "§l" + name + ":\n" + 
        "§0Type: §8" + act + "\n" + 
        "§0" + kind + ": §8" + getIdName(id) + "\n";
      if ((!aPage.contains("\n")) || (aPage.split("\n").length < 9))
      {
        aPage = aPage + " \n" + page;
      }
      else
      {
        pages.add(aPage);
        aPage = page;
      }
    }
    if (!aPage.equals("")) {
      pages.add(aPage);
    }
    m.setPages(pages);
    book.setItemMeta(m);
  }
  
  private int getNewID(String type)
  {
    int i = 0;
    int id;
    do
    {
      id = rand(1, 999999999);
      i++;
    } while ((i <= 255) && 
    
      (this.saveFile.getString(type + "." + id) != null));
    return id;
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onEntityDeath(EntityDeathEvent e){
	  LivingEntity v = e.getEntity();
	  if ((v.getKiller() instanceof Player)) {
		  for (String et :(ArrayList<String>) getConfig().getList("spellBook.drops")) {
			  if ((isMob(v, et)) && (rand(1, 100) <= getConfig().getInt("spellBook.chance"))) {
				  e.getDrops().add(getSpellBook());
			  }
		  }
	  }
	  for (String mob : getConfig().getConfigurationSection("mobDrops").getKeys(false)) {
		  if (isMob(v, mob)){
			  for (String i : getConfig().getConfigurationSection("mobDrops." + v.getType().toString().toLowerCase().replace("_", "-")).getKeys(false)) {
				  int c = getConfig().getInt("mobDrops." + v.getType().toString().toLowerCase().replace("_", "-") + "." + i + ".chance");
				  if (rand(1, 100) <= c){
					  ItemStack drop = getItem("mobDrops." + v.getType().toString().toLowerCase().replace("_", "-") + "." + i, getConfig());
					  e.getDrops().add(drop);
				  }
			  }
			  break;
		  }
	  }
  }
  
  private boolean isMob(Entity e, String mob){
	  if (e.getType().equals(EntityType.SKELETON)){
		  Skeleton sk = (Skeleton)e;
		  if (sk.getType().equals(Skeleton.SkeletonType.WITHER)) {
			  if(mob.equalsIgnoreCase("WITHER_SKELETON") || (mob.equalsIgnoreCase("WITHER-SKELETON"))){
				  return true;
			  }else
				  return false;
		  }
	  }else if (e.getType().equals(EntityType.GUARDIAN)){
		  Guardian gu = (Guardian)e;
		  if (gu.isElder()) {
			  if((mob.equalsIgnoreCase("ELDER_GUARDIAN")) || (mob.equalsIgnoreCase("ELDER-GUARDIAN"))){
				  return true;
			  }else
				  return false;
		  }
	  }else if (e.getType().toString().toLowerCase().replace("_", "-").equalsIgnoreCase(mob)){
		  return true;
	  }
	  return false;
  }
  
  private void openSpellBook(Player p, int id)
  {
    Inventory inv = getServer().createInventory(p, 9, "§0§lSpell Book");
    for (int i = 0; i < 9; i++) {
      if (this.saveFile.getString("spellBooks." + id + "." + i) != null) {
        inv.setItem(i, getSpell(this.saveFile.getString("spellBooks." + id + "." + i)));
      }
    }
    this.spellBooks.put(p.getName(), Integer.valueOf(id));
    p.openInventory(inv);
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onInventoryClose(InventoryCloseEvent e)
  {
    Player p = (Player)e.getPlayer();
    if (e.getInventory().getName().equals("Spell Altar"))
    {
      saveInv(e.getInventory(), ((Block)this.altarMap.get(p.getName())).getLocation());
      this.altarMap.remove(p.getName());
    }
    else if ((e.getInventory().getName().equals("§0§lSpell Book")) && (this.spellBooks.get(p.getName()) != null))
    {
      int id = ((Integer)this.spellBooks.get(p.getName())).intValue();
      for (int i = 0; i < 9; i++) {
        try
        {
          if (e.getInventory().getItem(i) != null)
          {
            String spell = e.getInventory().getItem(i).getItemMeta().getDisplayName().replace(" §5Spell", "").replace("§5", "");
            this.saveFile.set("spellBooks." + id + "." + i, spell);
          }
          else
          {
            this.saveFile.set("spellBooks." + id + "." + i, null);
          }
        }
        catch (Exception localException) {}
      }
      try
      {
        this.saveFile.save(this.saveYML);
      }
      catch (IOException localIOException) {}
      this.spellBooks.remove(p.getName());
    }
    else if ((e.getInventory().getName().equals("§0§lBind Staff")) && (this.staffs.get(p.getName()) != null))
    {
      int uuid = ((Integer)this.staffs.get(p.getName())).intValue();
      int nxt = 0;
      for (int i = 0; i < 9; i++) {
        try
        {
          if ((e.getInventory().getItem(i) != null) && (!e.getInventory().getItem(i).getItemMeta().getDisplayName().equals("§f")))
          {
            String spell = e.getInventory().getItem(i).getItemMeta().getDisplayName().replace(" §5Spell", "").replace("§5", "");
            this.saveFile.set("staffs." + uuid + "." + nxt, spell);
            nxt++;
          }
        }
        catch (Exception localException1) {}
      }
      try
      {
        this.saveFile.save(this.saveYML);
      }
      catch (IOException localIOException1) {}
      this.spellBooks.remove(p.getName());
      if (nxt > 0) {
        p.sendMessage("§4WMagic: §dYour staff has been bound!");
      } else {
        p.sendMessage("§4WMagic: §dYou did not bind your staff!");
      }
    }
  }
  
  private void saveInv(Inventory i, Location l)
  {
    String name = getLocationName(l);
    this.saveFile.set("altars." + name, null);
    for (int j = 0; j < i.getSize(); j++)
    {
      ItemStack s = i.getItem(j);
      if ((s != null) && (!s.getType().equals(Material.AIR))) {
        setItem(s, "altars." + name + "." + j, this.saveFile);
      }
    }
    try
    {
      this.saveFile.save(this.saveYML);
    }
    catch (IOException localIOException) {}
  }
  
  private void loadInv(Inventory i, Location l)
  {
    String name = getLocationName(l);
    if (this.saveFile.getString("altars." + name) != null) {
      for (int j = 0; j < i.getSize(); j++)
      {
        ItemStack s = getItem("altars." + name + "." + j, this.saveFile);
        if ((s != null) && (!s.getType().equals(Material.AIR))) {
          i.setItem(j, s);
        }
      }
    }
  }
  
  private int getAltarLevel(Block b)
  {
    Location l = b.getLocation();
    int r = 0;
    for (double x = l.getX() - 1.0D; x <= l.getX() + 1.0D; x += 1.0D) {
      for (double z = l.getZ() - 1.0D; z <= l.getZ() + 1.0D; z += 1.0D)
      {
        Location tmp = new Location(l.getWorld(), x, l.getY(), z);
        if (tmp.getBlock().getType().equals(Material.REDSTONE_WIRE)) {
          r++;
        }
      }
    }
    if (r == 8)
    {
      Material mat = null;
      int m = 0;
      for (double x = l.getX() - 1.0D; x <= l.getX() + 1.0D; x += 1.0D) {
        for (double z = l.getZ() - 1.0D; z <= l.getZ() + 1.0D; z += 1.0D)
        {
          Location tmp = new Location(l.getWorld(), x, l.getY() - 1.0D, z);
          if (mat == null)
          {
            mat = tmp.getBlock().getType();
            m++;
          }
          else if (tmp.getBlock().getType().equals(mat))
          {
            m++;
          }
        }
      }
      if (m == 9)
      {
        if (mat.equals(Material.DIAMOND_BLOCK)) {
          return 3;
        }
        if (mat.equals(Material.GOLD_BLOCK)) {
          return 2;
        }
        if (mat.equals(Material.IRON_BLOCK)) {
          return 1;
        }
        return 0;
      }
    }
    return -1;
  }
  
  private boolean setSpellSymbol(ItemStack m)
  {
    short id = Bukkit.getMap(m.getDurability()).getId();
    if (!this.renderedMaps.contains(Short.valueOf(id)))
    {
      String spell = m.getItemMeta().getDisplayName().replace(" §5Spell", "").replace("§5", "");
      MapView map = Bukkit.createMap((World)Bukkit.getWorlds().get(0));
      for (MapRenderer mr : map.getRenderers())
      {
        if (((mr instanceof imgMapRenderer)) && (((imgMapRenderer)mr).imgName.equals(spell + ".png"))) {
          return false;
        }
        map.removeRenderer(mr);
      }
      imgMapRenderer r = new imgMapRenderer();
      r.setImage(spell + ".png");
      map.addRenderer(r);
      
      m.setDurability(map.getId());
      this.renderedMaps.add(Short.valueOf(map.getId()));
      return true;
    }
    return false;
  }
  
  @EventHandler(priority=EventPriority.HIGHEST)
  public void onPlayerInteract(PlayerInteractEntityEvent e)
  {
    Player p = e.getPlayer();
    if ((e.getRightClicked().getType().equals(EntityType.ITEM_FRAME)) && 
      (p.getItemInHand().getTypeId() == 358) && (p.getItemInHand().getItemMeta().getDisplayName().contains("§5Spell")))
    {
      e.setCancelled(true);
      p.sendMessage("§4WMagic: §dYou can't put spells in item frames!");
    }
    if ((e.getRightClicked() instanceof LivingEntity)) {
      interact(p, null, Action.RIGHT_CLICK_AIR, (LivingEntity)e.getRightClicked());
    }
  }
  
  private String getIdName(String id)
  {
    String[] s = id.split(":");
    ItemStack i = new ItemStack(Integer.parseInt(s[0]), 1, (byte)Integer.parseInt(s[1]));
    return getIdName(i, id);
  }
  
  private String getIdName(ItemStack i, String id){
    if (i.getTypeId() == 397)
    {
      if (i.getDurability() == 0) {
        return "Skeleton Skull";
      }
      if (i.getDurability() == 1) {
        return "Wither Skull";
      }
      if (i.getDurability() == 2) {
        return "Zombie Skull";
      }
      if (i.getDurability() == 3) {
        return "Player Skull";
      }
      if (i.getDurability() == 4) {
        return "Creeper Skull";
      }
    }
    if ((i.getItemMeta() != null) && (i.getItemMeta().getDisplayName() != null)) {
      return i.getItemMeta().getDisplayName();
    }
    try
    {
      if (Bukkit.getServer().getPluginManager().getPlugin("Vault") != null) {
        return Items.itemByStack(i).getName();
      }
    }
    catch (Exception localException){}
      if (i.getTypeId() == 373) {
        return getPotionName(Integer.parseInt(id.split(":")[1]));
      }
      if ((i.getTypeId() == 322) && (i.getDurability() == 1)) {
        return "Notch Apple";
      }
      String o = i.getType().toString().toLowerCase().replace("_", " ");
      return o.substring(0, 1).toUpperCase() + o.substring(1);
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onPlayerInteract(PlayerInteractEvent e){
	  Player p = e.getPlayer();
	  if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
		  Block b = e.getClickedBlock();
		  if ((b.getType().equals(Material.ENCHANTMENT_TABLE)) && (getAltarLevel(b) >= 0)){
			  if(getConfig().getBoolean("allUseNeedsPerm") && (!p.hasPermission(getConfig().getString("allUsePerm")))){
				  p.sendMessage("§4WMagic: §dYou can't do that!");
				  return;
			  }
			  //In Use
			  for (Map.Entry<String, Block> i : this.altarMap.entrySet()) {
				  if (((Block)i.getValue()).equals(e.getClickedBlock())){
					  e.setCancelled(true);
					  p.sendMessage("§4WMagic: §dAnother player is using that altar!");
					  return;
				  }
			  }
			  e.setCancelled(true);
			  this.altarMap.put(p.getName(), e.getClickedBlock());
			  Inventory inv = getServer().createInventory(p, InventoryType.DISPENSER, "Spell Altar");
			  p.openInventory(inv);
			  loadInv(inv, e.getClickedBlock().getLocation());
			  p.updateInventory();
			  return;
		  }
	  }
	  try{
		  if ((p.getItemInHand().getTypeId() == 395) && (p.getItemInHand().getItemMeta().getDisplayName().equals(getConfig().getString("arcaneScroll.name")))) {
			  e.setCancelled(true);
		  }
	  }catch (Exception localException) {}
	  if (!e.getAction().equals(Action.PHYSICAL)){
		  Block b = e.getClickedBlock();
		  if ((b == null) || ((!b.getType().toString().toLowerCase().contains("door")) && (!b.getType().toString().toLowerCase().contains("chest")) && (!b.getType().toString().toLowerCase().contains("button")) && (!b.getType().toString().toLowerCase().contains("lever")))) {
			  interact(p, e.getClickedBlock(), e.getAction(), null);
		  }
	  }
	  if (e.getClickedBlock() != null) {
		  checkKnowledge(p, e.getClickedBlock().getTypeId() + ":" + e.getClickedBlock().getData(), "interact", true);
	  }
  }
  
  private String getName(String uuid){
	  return saveFile.getString("players."+uuid+".name");
  }
  
  private boolean interact(Player p, Block b, Action a, LivingEntity e){
	  if(getConfig().getBoolean("allUseNeedsPerm") && (!p.hasPermission(getConfig().getString("allUsePerm")))){
		  p.sendMessage("§4WMagic: §dYou can't do that!");
		  return false;
	  }
	  boolean ret = false;
	  try{
		  if (p.getItemInHand().getItemMeta().getDisplayName().contains(getConfig().getString("spellBook.name").replace("&", "§"))){
			  int id = Integer.parseInt(((String)p.getItemInHand().getItemMeta().getLore().get(0)).replace("§k", ""));
			  if (this.saveFile.getString("spellBooks." + id) == null){
				  this.saveFile.set("spellBooks." + id + ".owner", p.getUniqueId().toString());
				  try{
					  this.saveFile.save(this.saveYML);
				  }catch (IOException localIOException) {}
			  }
			  if (p.isSneaking()){
				  if ((a.equals(Action.RIGHT_CLICK_BLOCK)) || (a.equals(Action.RIGHT_CLICK_AIR))){
					  String owner = this.saveFile.getString("spellBooks." + id + ".owner");
					  if ((p.getUniqueId().toString().equals(owner)) || (!getConfig().getBoolean("protectedSpellBooks"))) {
						  openSpellBook(p, id);
					  } else {
						  p.sendMessage("§4WMagic: §dThis book is bound to "+getName(owner)+" with a protection spell!");
					  }
				  }else if ((a.equals(Action.LEFT_CLICK_BLOCK)) || (a.equals(Action.LEFT_CLICK_AIR))){
					  String spell = "nothing";
					  String[] oldSpells = new String[9];
					  for (int i = 0; i < 9; i++) {
						  oldSpells[i] = this.saveFile.getString("spellBooks." + id + "." + i);
					  }
					  for (int i = 0; i < 9; i++) {
						  int index = i + 1;
						  if (index > 8) {
							  index = 0;
							  spell = oldSpells[i];
						  }
						  this.saveFile.set("spellBooks." + id + "." + index, oldSpells[i]);
					  }
					  try{
						  this.saveFile.save(this.saveYML);
					  }catch (IOException localIOException1) {}
					  if (spell == null) {
						  spell = "nothing";
					  }
					  p.sendMessage("§4WMagic: §dCast spell set to " + spell + "!");
				  }
			  }else if (this.saveFile.getString("spellBooks." + id + ".0") != null){
				  String spell = "none";
				  for(int i = 0; i < 9; i++){
					  String s = saveFile.getString("spellBooks." + id + "." + i);
					  if(s != null){
						  spell = s;
						  break;
					  }
				  }
				  if (castSpell(p, e, b, spell)){
					  if (rand(1, 100) <= getConfig().getInt("manaOnCastChance")) {
						  addMana(p, 1);
					  }
					  ret = true;
					  if (getConfig().getString("spells." + spell + ".cooldown") != null) {
						  cool(p.getUniqueId(), spell);
					  }
				  }
			  } else {
				  p.sendMessage("§4WMagic: §dNo spell found!");
			  }
		  }else if ((p.getItemInHand().getTypeId() == 358) && (p.getItemInHand().getItemMeta().getDisplayName().contains("§5Spell"))){
        String spell = p.getItemInHand().getItemMeta().getDisplayName().replace(" §5Spell", "").replace("§5", "");
        if (castSpell(p, e, b, spell))
        {
          if (rand(1, 100) <= getConfig().getInt("manaOnCastChance")) {
            addMana(p, 1);
          }
          ret = true;
          if (getConfig().getString("spells." + spell + ".cooldown") != null) {
            cool(p.getUniqueId(), spell);
          }
        }
      }
      else
      {
        String[] s = ((String)p.getItemInHand().getItemMeta().getLore().get(0)).replace("§k", "").split(":");
        int uuid = Integer.parseInt(s[0]);
        String id = s[1];
        if (this.saveFile.getString("staffs." + uuid) == null)
        {
          int slots = getConfig().getInt("staffs." + id + ".slots");
          openStaff(p, uuid, slots);
        }
        else if ((a.equals(Action.RIGHT_CLICK_BLOCK)) || (a.equals(Action.RIGHT_CLICK_AIR)))
        {
          if (this.saveFile.getString("staffs." + uuid + ".0") != null)
          {
            String spell = this.saveFile.getString("staffs." + uuid + ".0");
            if (castSpell(p, e, b, spell))
            {
              if (rand(1, 100) <= getConfig().getInt("manaOnCastChance")) {
                addMana(p, 1);
              }
              ret = true;
              if (getConfig().getString("spells." + spell + ".cooldown") != null) {
                cool(p.getUniqueId(), spell);
              }
            }
          }
          else
          {
            p.sendMessage("§4WMagic: §dNo spell found!");
          }
        }
        else if ((a.equals(Action.LEFT_CLICK_BLOCK)) || (a.equals(Action.LEFT_CLICK_AIR)))
        {
          String spell = "nothing";
          int slots = this.saveFile.getConfigurationSection("staffs." + uuid).getKeys(false).size();
          String[] oldSpells = new String[slots];
          for (int i = 0; i < slots; i++) {
            oldSpells[i] = this.saveFile.getString("staffs." + uuid + "." + i);
          }
          for (int i = 0; i < slots; i++)
          {
            int index = i + 1;
            if (index > slots - 1)
            {
              index = 0;
              spell = oldSpells[i];
            }
            this.saveFile.set("staffs." + uuid + "." + index, oldSpells[i]);
          }
          try
          {
            this.saveFile.save(this.saveYML);
          }
          catch (IOException localIOException2) {}
          if (spell == null) {
            spell = "nothing";
          }
          p.sendMessage("§4WMagic: §dCast spell set to " + spell + "!");
        }
      }
    }
    catch (Exception localException) {}
    return ret;
  }
  
  private void openStaff(Player p, int id, int slots)
  {
    Inventory inv = getServer().createInventory(p, 9, "§0§lBind Staff");
    for (int i = slots; i < 9; i++) {
      try
      {
        ItemStack n = getItem("160", "§f", 1, null);
        inv.setItem(i, n);
      }
      catch (Exception localException) {}
    }
    this.staffs.put(p.getName(), Integer.valueOf(id));
    p.openInventory(inv);
  }
  
  private boolean castSpell(Player p, LivingEntity e, Block b, String spell){
	  try{
		  if (castCool.contains(p.getName())) {
			  return false;
		  }
		  castCool.add(p.getName());
		  castCool(p);
		  //System.out.println("Spell: " + (getConfig().getString("spells." + spell) != null));
		  if (getConfig().getString("spells." + spell) != null){
			  if ((getConfig().getString("spells." + spell + ".perms") != null) && (!p.hasPermission(getConfig().getString("spells." + spell + ".perms")))){
				  p.sendMessage("§4WMagic: §dYou do not have permission to use this spell!");
				  return false;
			  }
			  try{
				  Location bl = p.getLocation();
				  if(b != null)
					  bl = b.getLocation();
				  if ((isBreakSpell(spell)) && (!canBuild(p, bl))){
					  p.sendMessage("§4WMagic: §dYou do not have permission to use that here!");
					  return false;
				  }
				  if ((isAttackSpell(spell)) && (!canAttack(p, p.getLocation()))){
					  p.sendMessage("§4WMagic: §dPVP is disabled here!");
					  return false;
				  }
			  }catch(Exception x){x.printStackTrace();}
			  String type = getConfig().getString("spells." + spell + ".type");
			  //System.out.println("Type: " + type);
			  if (((p.getItemInHand().getTypeId() == 395) || (p.getItemInHand().getTypeId() == 358)) && (setSpellSymbol(p.getItemInHand()))) {
				  p.updateInventory();
			  }
			  //System.out.println("Type: " + type);
			  if ((type.equals("touch"))) {
				  return effectSpell(p, e, b, spell);
			  }
			  if (type.equals("projectile")) {
				  return rangedSpell(p, spell);
			  }
			  if (type.equals("self")) {
				  return effectSpell(p, p, null, spell);
			  }
		  }
		  p.sendMessage("§4WMagic: §dSpell invalid!");
	  }catch (Exception x){x.printStackTrace();}
	  return false;
  }
  
  private void checkKnowledge(Player p, String id, String type, boolean addMana)
  {
    try
    {
      if (getConfig().getString("knowledge." + type + "." + id) != null)
      {
        if (getConfig().getString("knowledge." + type + "." + id + ".perm") != null)
        {
          String perm = getConfig().getString("knowledge." + type + "." + id + ".perm");
          if (!p.hasPermission(perm)) {
            return;
          }
        }
        String name = getConfig().getString("knowledge." + type + "." + id + ".name");
        ArrayList<String> kl = getKnowledge(p);
        if (!kl.contains(name))
        {
          String msg = getConfig().getString("knowledgeMsg").replace("<name>", name).replace("&", "§");
          p.sendMessage(msg);
          if (addMana)
          {
            int mana = getConfig().getInt("knowledge." + type + "." + id + ".mana");
            addMana(p, mana);
          }
          kl.add(name);
          this.saveFile.set("players." + p.getUniqueId().toString() + ".knowledge", kl);
          try
          {
            this.saveFile.save(this.saveYML);
          }
          catch (IOException localIOException) {}
        }
      }
      return;
    }
    catch (Exception x)
    {
      x.printStackTrace();
    }
  }
  
  private ArrayList<String> getKnowledge(Player p)
  {
    ArrayList<String> kl = new ArrayList();
    if (this.saveFile.getList("players." + p.getUniqueId().toString() + ".knowledge") != null) {
      kl = (ArrayList)this.saveFile.getList("players." + p.getUniqueId().toString() + ".knowledge");
    }
    return kl;
  }
  
  private ArrayList<String> getAllKnowledge(){
	  ArrayList<String> kl = new ArrayList();
	  //Iterator localIterator2;
	  for(String type : getConfig().getConfigurationSection("knowledge").getKeys(false))
		  for(String id : getConfig().getConfigurationSection("knowledge." + type).getKeys(false))
			  kl.add(getConfig().getString("knowledge." + type + "." + id + ".name"));
	  return kl;
  }
  
  private String getKnowledgeID(String name){
	  for(String type : getConfig().getConfigurationSection("knowledge").getKeys(false))
		  for(String id : getConfig().getConfigurationSection("knowledge." + type).getKeys(false))
			  if (getConfig().getString("knowledge." + type + "." + id + ".name").equals(name)) 
				  return id;
	  return null;
  }
  
  private String getKnowledgeType(String name){
	  for (String type : getConfig().getConfigurationSection("knowledge").getKeys(false))
		  for(String id : getConfig().getConfigurationSection("knowledge." + type).getKeys(false))
			  if (getConfig().getString("knowledge." + type + "." + id + ".name").equals(name))
				  return type;
	  return null;
  }
  
  private void addMana(Player p, int amount)
{
    int max = getConfig().getInt("startingMana");
    if (this.saveFile.getString("players." + p.getUniqueId().toString() + ".mana") != null) {
      max = this.saveFile.getInt("players." + p.getUniqueId().toString() + ".mana");
    }
    this.saveFile.set("players." + p.getUniqueId().toString() + ".mana", Integer.valueOf(max + amount));
    try
    {
      this.saveFile.save(this.saveYML);
    }
    catch (IOException localIOException) {}
  }
  
  private void setMana(Player p, int amount)
  {
    this.saveFile.set("players." + p.getUniqueId().toString() + ".mana", Integer.valueOf(amount));
    try
    {
      this.saveFile.save(this.saveYML);
    }
    catch (IOException localIOException) {}
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onPlayerDropItem(PlayerDropItemEvent e){
    Player p = e.getPlayer();
    Location l = p.getLocation();
    if ((blockNear(l, Material.ENCHANTMENT_TABLE, 2) != null) && (getAltarLevel(blockNear(l, Material.ENCHANTMENT_TABLE, 2)) >= 0)) {
      int rlvl;
      if (isArcaneScroll(e.getItemDrop().getItemStack())){
    	    if(getConfig().getBoolean("allUseNeedsPerm") && (!p.hasPermission(getConfig().getString("allUsePerm")))){
    	    	p.sendMessage("§4WMagic: §dYou can't do that!");
    	    	return;
    	    }
        String spell = null;
        Block et = blockNear(l, Material.ENCHANTMENT_TABLE, 2);
        int lvl = getAltarLevel(et);
        ItemStack[] r = getAltarItems(et.getLocation());
        ItemStack[] sr;
        for (String sp : getConfig().getConfigurationSection("spells").getKeys(false))
        {
          sr = getRecipe("spells", sp);
          if (itemsAreSame(r, sr))
          {
            spell = sp;
            break;
          }
        }
        if (spell != null)
        {
          rlvl = getConfig().getInt("spells." + spell + ".altarLevel");
          if (lvl >= rlvl)
          {
            if ((getConfig().getString("spells." + spell + ".perms") != null) && 
              (!p.hasPermission(getConfig().getString("spells." + spell + ".perms"))))
            {
              p.sendMessage("§4WMagic: §dYou do not have permission to craft this spell!");
              return;
            }
            if (getConfig().getList("spells." + spell + ".requiredKnowledge") != null) {
              for (String k : (ArrayList<String>)getConfig().getList("spells." + spell + ".requiredKnowledge")) {
                if (!getKnowledge(p).contains(k))
                {
                  p.sendMessage("§4WMagic: §dYou have not discovered §5" + k + "§d!");
                  return;
                }
              }
            }
            String name = getLocationName(et.getLocation());
            this.saveFile.set("altars." + name, null);
            try
            {
              this.saveFile.save(this.saveYML);
            }
            catch (IOException localIOException) {}
            e.getItemDrop().getWorld().dropItemNaturally(e.getItemDrop().getLocation(), getSpell(spell));
            et.getWorld().strikeLightningEffect(et.getLocation());
            e.getItemDrop().remove();
            p.sendMessage("§4WMagic: §dYou have crafted the spell " + spell + "§d!");
          }
          else
          {
            p.sendMessage("§4WMagic: §dAltar level too low for that spell!");
          }
        }
        else
        {
          p.sendMessage("§4WMagic: §dAltar's item arrangement not recognized!");
        }
      }else if (isBindingPowder(e.getItemDrop().getItemStack())){
    	    if(getConfig().getBoolean("allUseNeedsPerm") && (!p.hasPermission(getConfig().getString("allUsePerm")))){
    	    	p.sendMessage("§4WMagic: §dYou can't do that!");
    	    	return;
    	    }
        Block et = blockNear(l, Material.ENCHANTMENT_TABLE, 2);
        int lvl = getAltarLevel(et);
        ItemStack[] r = getAltarItems(et.getLocation());
        for (String sp : getConfig().getConfigurationSection("staffs").getKeys(false))
        {
          ItemStack[] sr = getRecipe("staffs", sp);
          if (itemsAreSame(r, sr))
          {
            if (lvl >= getConfig().getInt("staffs." + sp + ".altarLevel"))
            {
              String name = getLocationName(et.getLocation());
              this.saveFile.set("altars." + name, null);
              try
              {
                this.saveFile.save(this.saveYML);
              }
              catch (IOException localIOException1) {}
              e.getItemDrop().getWorld().dropItemNaturally(e.getItemDrop().getLocation(), getStaff(sp));
              
              et.getWorld().strikeLightningEffect(et.getLocation());
              e.getItemDrop().remove();
              p.sendMessage("§4WMagic: §dYou have crafted a staff!");
            }
            else
            {
              p.sendMessage("§4WMagic: §dAltar level too low for that staff!");
            }
            return;
          }
        }
        for (String sp : getConfig().getConfigurationSection("staffCaps").getKeys(false))
        {
          ItemStack[] sr = getRecipe("staffCaps", sp);
          if (itemsAreSame(r, sr))
          {
            if (lvl >= getConfig().getInt("staffCaps." + sp + ".altarLevel"))
            {
              String name = getLocationName(et.getLocation());
              this.saveFile.set("altars." + name, null);
              try
              {
                this.saveFile.save(this.saveYML);
              }
              catch (IOException localIOException2) {}
              e.getItemDrop().getWorld().dropItemNaturally(e.getItemDrop().getLocation(), getItem("staffCaps." + sp, getConfig()));
              
              et.getWorld().strikeLightningEffect(et.getLocation());
              e.getItemDrop().remove();
              p.sendMessage("§4WMagic: §dYou have crafted a staff cap!");
            }
            else
            {
              p.sendMessage("§4WMagic: §dAltar level too low for that staff!");
            }
            return;
          }
        }
        p.sendMessage("§4WMagic: §dAltar's item arrangement not recognized!");
      }
    }
    if (isBindingPowder(e.getItemDrop().getItemStack()))
    {
      final Item pow = e.getItemDrop();
      Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
      {
        public void run()
        {
          WizardlyMagic.this.manaPotionBrew(pow);
        }
      }, 40L);
    }
  }
  
  private void manaPotionBrew(final Item powder)
  {
    Item bottle = null;
    Item m = null;
    int lvl = 0;
    for (Entity e : powder.getNearbyEntities(2.0D, 2.0D, 2.0D)) {
      if ((e instanceof Item)) {
        if (((Item)e).getItemStack().getType().equals(Material.GLASS_BOTTLE))
        {
          bottle = (Item)e;
        }
        else if (((Item)e).getItemStack().getType().equals(Material.SLIME_BALL))
        {
          lvl = 1;
          m = (Item)e;
        }
        else if (((Item)e).getItemStack().getType().equals(Material.MAGMA_CREAM))
        {
          lvl = 2;
          m = (Item)e;
        }
        else if (((Item)e).getItemStack().getType().equals(Material.EYE_OF_ENDER))
        {
          lvl = 3;
          m = (Item)e;
        }
      }
    }
    if ((bottle != null) && (lvl > 0))
    {
      Location loc = powder.getLocation();
      ItemStack mp = getManaPotion(lvl);
      powder.getWorld().dropItem(powder.getLocation(), mp);
      if (powder.getItemStack().getAmount() == 1) {
        powder.remove();
      } else {
        powder.getItemStack().setAmount(powder.getItemStack().getAmount() - 1);
      }
      if (bottle.getItemStack().getAmount() == 1) {
        bottle.remove();
      } else {
        bottle.getItemStack().setAmount(bottle.getItemStack().getAmount() - 1);
      }
      if (m.getItemStack().getAmount() == 1) {
        m.remove();
      } else {
        m.getItemStack().setAmount(m.getItemStack().getAmount() - 1);
      }
      displayParticle("WITCH_MAGIC", loc.getWorld(), loc.getX(), loc.getY(), loc.getZ(), 0.5D, 0, 10);
      if ((!bottle.isDead()) && (!powder.isDead())) {
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
        {
          public void run()
          {
            WizardlyMagic.this.manaPotionBrew(powder);
          }
        }, 20L);
      }
    }
  }
  
  private boolean itemsAreSame(ItemStack[] r1, ItemStack[] r2)
  {
    if ((r1 == null) || (r2 == null)) {
      return false;
    }
    for (int i = 0; i < 9; i++) {
      if (((r1[i] == null) && (r2[i] != null)) || ((r1[i] != null) && (r2[i] == null)) || ((r1[i] != null) && (r2[i] != null) && (!r1[i].equals(r2[i])))) {
        return false;
      }
    }
    return true;
  }
  
  private ItemStack[] getRecipe(String type, String id)
  {
    ItemStack[] r = new ItemStack[9];
    for (int i = 0; i < 9; i++)
    {
      ItemStack s = null;
      if (getConfig().getString(type + "." + id + ".recipe." + i) != null) {
        s = getItem(type + "." + id + ".recipe." + i, getConfig());
      }
      if ((s != null) && 
        (s.getType().equals(Material.AIR))) {
        s = null;
      }
      r[i] = s;
    }
    return r;
  }
  
  private ItemStack[] getAltarItems(Location l)
  {
    ItemStack[] r = new ItemStack[9];
    String name = getLocationName(l);
    if (this.saveFile.getString("altars." + name) != null) {
      for (int i = 0; i < 9; i++)
      {
        ItemStack s = getItem("altars." + name + "." + i, this.saveFile);
        if ((s != null) && (!s.getType().equals(Material.AIR))) {
          r[i] = s;
        } else {
          r[i] = null;
        }
      }
    }
    return r;
  }
  
  private boolean takeMana(Player p, String spell){
	  int pMana = getPlayerMana(p);
	  int sMana = getConfig().getInt("spells." + spell + ".mana");
	  int max = getMaxMana(p.getUniqueId().toString());
	  if (pMana >= sMana){
		  if ((sMana < 0) && (pMana - sMana > max))
			  return false;
		  playerMana.put(p.getName(), Integer.valueOf(pMana - sMana));
		  updateManaBar(p);
		  return true;
	  }
    return false;
  }
  
  private int getPlayerMana(Player p)
  {
    int pMana = 0;
    if (this.playerMana.get(p.getName()) != null)
    {
      pMana = ((Integer)this.playerMana.get(p.getName())).intValue();
    }
    else if (this.saveFile.getString("players." + p.getUniqueId().toString() + ".mana") != null)
    {
      pMana = this.saveFile.getInt("players." + p.getUniqueId().toString() + ".mana");
    }
    else
    {
      pMana = getConfig().getInt("startingMana");
      this.saveFile.set("players." + p.getUniqueId().toString() + ".mana", Integer.valueOf(pMana));
      try
      {
        this.saveFile.save(this.saveYML);
      }
      catch (IOException localIOException) {}
    }
    return pMana;
  }
  
  private boolean effectSpell(Player p, LivingEntity target, Block b, String spell){
	  try{
		  if ((this.coolMap.get(p.getUniqueId()) != null) && (((String)this.coolMap.get(p.getUniqueId())).equals(spell))){
			  p.sendMessage("§cSpell is cooling!");
			  return false;
		  }
		  if (!takeMana(p, spell)){
			  p.sendMessage("§4WMagic: §dYou don't have enough mana!");
			  return false;
		  }
		  if (getConfig().getString("spells." + spell + ".sound") != null) {
			  p.getWorld().playSound(p.getLocation(), Sound.valueOf(getConfig().getString("spells." + spell + ".sound")), 1.0F, 1.0F);
		  }
		  if ((getConfig().getBoolean("spells." + spell + ".effects.recall")) && (target != null)) {
			  try{
				  target.teleport(getLocation("players." + p.getUniqueId().toString() + ".mark", this.saveFile));
			  }catch (Exception localException1) {}
		  }
		  doSelfEffects(p, spell);
      
		  int damage = getConfig().getInt("spells." + spell + ".effects.damage");
		  if (target != null) {
			  effect(target, damage, getPotions("spells." + spell + ".effects.potionEffects"), getPotions("spells." + spell + ".effects.clearPotionEffects"));
		  }
		  //System.out.println("SSS");
		  if (b != null){
			  int dig = getConfig().getInt("spells." + spell + ".effects.dig");
			  int power = getConfig().getInt("spells." + spell + ".effects.digPower");
			  boolean rep = getConfig().getBoolean("spells." + spell + ".effects.digReplace");
			  if (dig > 0) {
				  //System.out.println("Dig");
				  dig(p, rep, b.getLocation(), dig, power);
			  }
		  }
		  if ((target != null) && (getConfig().getBoolean("spells." + spell + ".effects.mount"))) {
			  target.setPassenger(p);
		  }
		  Location l = p.getLocation();
		  if (target != null) {
			  l = target.getLocation();
		  } else if (b != null) {
			  l = b.getLocation();
		  }
		  splashLocation(l, spell, p);
		  return true;
	  }catch (Exception x){x.printStackTrace();}
	  return false;
  }
  
  private void doSelfEffects(Player p, String spell){
	  if (getConfig().getBoolean("spells." + spell + ".selfEffects.recall")) {
		  try{
			  p.teleport(getLocation("players." + p.getUniqueId().toString() + ".mark", this.saveFile));
		  }catch (Exception localException) {}
	  }
	  //command
	  if(getConfig().getList("spells." + spell + ".selfEffects.commands") != null){
		  for(String cmd : (ArrayList<String>)getConfig().getList("spells." + spell + ".selfEffects.commands"))
			  runCommand(cmd,p.getLocation(),p,getConfig().getBoolean("spells." + spell + ".selfEffects.playerCommand") );
	  }
	  int damage = getConfig().getInt("spells." + spell + ".selfEffects.damage");
	  effect(p, damage, getPotions("spells." + spell + ".selfEffects.potionEffects"), getPotions("spells." + spell + ".selfEffects.clearPotionEffects"));
	  if (getConfig().getString("spells." + spell + ".selfEffects.velocity") != null){
		  String[] s = getConfig().getString("spells." + spell + ".selfEffects.velocity").split(":");
		  if (s[0].equals("forward")) {
			  p.setVelocity(p.getLocation().getDirection().multiply(Double.parseDouble(s[1])));
		  } else {
			  p.setVelocity(new Vector(Double.parseDouble(s[0]), Double.parseDouble(s[1]), Double.parseDouble(s[2])));
		  }
	  }
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onPlayerChangeItem(PlayerItemHeldEvent e)
  {
    Player p = e.getPlayer();
    try
    {
      if ((p.getInventory().getItem(e.getNewSlot()).getTypeId() == 358) && (p.getInventory().getItem(e.getNewSlot()).getItemMeta().getDisplayName().contains("§5Spell")) && 
        (setSpellSymbol(p.getInventory().getItem(e.getNewSlot())))) {
        p.updateInventory();
      }
    }
    catch (Exception localException) {}
    try
    {
      if (p.getItemInHand().getItemMeta().getDisplayName().equals(getConfig().getString("bookOfKnowledge.name").replace("&", "§"))) {
        p.getInventory().setItemInHand(getBookOfKnowledge(p));
      }
    }
    catch (Exception localException1) {}
  }
  
  private boolean isBreakSpell(String spell)
  {
    if (getConfig().getString("spells." + spell + ".spellType") != null)
    {
      String t = getConfig().getString("spells." + spell + ".spellType");
      if (t.equals("build")) {
        return true;
      }
      return false;
    }
    for (String s : getConfig().getConfigurationSection("spells." + spell).getKeys(true)) {
      if ((s.toLowerCase().contains("dig")) || (s.toLowerCase().contains("block"))) {
        return true;
      }
    }
    try
    {
      if (getConfig().getString("spells." + spell + ".projectile").equals("block")) {
        return true;
      }
    }
    catch (Exception localException) {}
    return false;
  }
  
  private boolean isAttackSpell(String spell)
  {
    for (String s : getConfig().getConfigurationSection("spells." + spell).getKeys(true))
    {
      s = s.toLowerCase();
      if (getConfig().getString("spells." + spell + ".spellType") != null)
      {
        String t = getConfig().getString("spells." + spell + ".spellType");
        if (t.equals("pvp")) {
          return true;
        }
        return false;
      }
      if ((s.contains("velocity")) || (s.contains("tnt")) || (s.contains("fire")) || (s.contains("damage")) || (s.contains("lightning")) || (s.contains("explode")) || (s.contains("disarm")) || (s.contains("poison")) || (s.contains("wither")) || (s.contains("harm"))) {
        return true;
      }
    }
    return false;
  }
  
  private boolean rangedSpell(final Player p, final String spell)
  {
    try
    {
      if (getConfig().getString("spells." + spell) != null)
      {
        if ((this.coolMap.get(p.getUniqueId()) != null) && (((String)this.coolMap.get(p.getUniqueId())).equals(spell)))
        {
          p.sendMessage("§cSpell is cooling!");
          return false;
        }
        if (!takeMana(p, spell))
        {
          p.sendMessage("§4WMagic: §dYou don't have enough mana!");
          return false;
        }
        doSelfEffects(p, spell);
        if (getConfig().getString("spells." + spell + ".sound") != null) {
          p.getWorld().playSound(p.getLocation(), Sound.valueOf(getConfig().getString("spells." + spell + ".sound")), 1.0F, 1.0F);
        }
        int projs = getConfig().getInt("spells." + spell + ".projectileCount");
        if (projs < 1) {
          projs = 1;
        }
        int interval = getConfig().getInt("spells." + spell + ".projectileInterval");
        
        Location s = null;
        final boolean fixed = getConfig().getBoolean("spells." + spell + ".fixedDrop");
        if (fixed) {
          s = getSkyLocation(p, spell);
        }
        final Location sky = s;
        for (int i = 0; i < projs; i++) {
          Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
          {
            public void run()
            {
              Location sky2 = sky;
              if (!fixed) {
                sky2 = WizardlyMagic.this.getSkyLocation(p, spell);
              }
              WizardlyMagic.this.launchProjectile(p, spell, sky2);
            }
          }, interval * i);
        }
        return true;
      }
      p.sendMessage("§4WMagic: §dSpell invalid!");
    }
    catch (Exception x)
    {
      x.printStackTrace();
    }
    return false;
  }
  
  private Location getSkyLocation(Player p, String spell)
  {
    int range = getConfig().getInt("spells." + spell + ".beamRange");
    boolean drop = getConfig().getBoolean("spells." + spell + ".drop");
    Location sky = null;
    if (drop)
    {
      int height = getConfig().getInt("spells." + spell + ".height");
      if (range > 0)
      {
        if (p.getTargetBlock((Set<Material>)null, range) != null) {
          sky = p.getTargetBlock((Set<Material>)null, range).getLocation();
        }
      }
      else {
        sky = p.getLocation();
      }
      sky.setY(sky.getY() + height);
      if (getConfig().getString("spells." + spell + ".randomRadius") != null)
      {
        double radius = getConfig().getDouble("spells." + spell + ".randomRadius");
        double distance = getConfig().getDouble("spells." + spell + ".randomDistance");
        if (distance <= 0.0D) {
          distance = 0.2D;
        }
        ArrayList<Location> ll = getFlatArea(sky.clone(), radius, distance);
        if (radius > 0.0D)
        {
          int index = new Random().nextInt(ll.size());
          sky = (Location)ll.get(index);
        }
      }
    }
    return sky;
  }
  
  private void launchProjectile(Player p, String spell, Location sky){
    String projType = getConfig().getString("spells." + spell + ".projectile");
    int life = getConfig().getInt("spells." + spell + ".lifeTime");
    int damage = 0;
    if (getConfig().getString("spells." + spell + ".effects.damage") != null) {
      damage = getConfig().getInt("spells." + spell + ".effects.damage");
    }
    if (getConfig().getString("spells." + spell + ".projectileSound") != null) {
      p.getWorld().playSound(p.getLocation(), Sound.valueOf(getConfig().getString("spells." + spell + ".projectileSound")), 1.0F, 1.0F);
    }
    boolean drop = getConfig().getBoolean("spells." + spell + ".drop");
    Entity proj = null;
    if (projType.equalsIgnoreCase("enderpearl"))
    {
      this.enderList.add(p.getUniqueId());
      if (drop) {
        proj = p.getWorld().spawnEntity(sky, EntityType.ENDER_PEARL);
      } else {
        proj = p.launchProjectile(EnderPearl.class);
      }
    }
    else if (projType.equalsIgnoreCase("snowball"))
    {
      if (drop) {
        proj = p.getWorld().spawnEntity(sky, EntityType.SNOWBALL);
      } else {
        proj = p.launchProjectile(Snowball.class);
      }
    }
    else if (projType.equalsIgnoreCase("egg"))
    {
      if (drop) {
        proj = p.getWorld().spawnEntity(sky, EntityType.EGG);
      } else {
        proj = p.launchProjectile(Egg.class);
      }
      this.eggList.add(proj.getUniqueId());
    }
    else if (projType.equalsIgnoreCase("arrow"))
    {
      if (drop) {
        proj = p.getWorld().spawnEntity(sky, EntityType.ARROW);
      } else {
        proj = p.launchProjectile(Arrow.class);
      }
      Arrow a = (Arrow)proj;
      if (getConfig().getBoolean("spells." + spell + ".critical")) {
        a.setCritical(true);
      }
    }
    else if (projType.equalsIgnoreCase("fireBall"))
    {
      if (drop) {
        proj = p.getWorld().spawnEntity(sky, EntityType.FIREBALL);
      } else {
        proj = p.launchProjectile(Fireball.class);
      }
    }
    else if (projType.equalsIgnoreCase("witherskull"))
    {
      if (drop) {
        proj = p.getWorld().spawnEntity(sky, EntityType.WITHER_SKULL);
      } else {
        proj = p.launchProjectile(WitherSkull.class);
      }
    }
    else if (projType.equalsIgnoreCase("item"))
    {
      boolean pu = getConfig().getBoolean("spells." + spell + ".allowItemPickup");
      if (drop)
      {
        if (pu) {
          proj = p.getWorld().dropItem(sky, getItem(getConfig().getString("spells." + spell + ".data"), null, 1, null));
        } else {
          proj = p.getWorld().dropItem(sky, getItem(getConfig().getString("spells." + spell + ".data"), "§7Thrown Grenade", 1, null));
        }
      }
      else if (pu) {
        proj = p.getWorld().dropItem(p.getEyeLocation(), getItem(getConfig().getString("spells." + spell + ".data"), null, 1, null));
      } else {
        proj = p.getWorld().dropItem(p.getEyeLocation(), getItem(getConfig().getString("spells." + spell + ".data"), "§7Thrown Grenade", 1, null));
      }
    }
    else if (projType.equalsIgnoreCase("block"))
    {
      String s = getConfig().getString("spells." + spell + ".data");
      String[] split = s.split(":");
      if (drop) {
        proj = p.getWorld().spawnFallingBlock(sky, Integer.parseInt(split[0]), (byte)Integer.parseInt(split[1]));
      } else {
        proj = p.getWorld().spawnFallingBlock(p.getEyeLocation(), Integer.parseInt(split[0]), (byte)Integer.parseInt(split[1]));
      }
    }
    else if (projType.equalsIgnoreCase("entity"))
    {
      EntityType et = EntityType.valueOf(getConfig().getString("spells." + spell + ".data").toUpperCase());
      if (drop) {
        proj = p.getWorld().spawnEntity(sky, et);
      } else {
        proj = p.getWorld().spawnEntity(p.getEyeLocation(), et);
      }
    }
    else if (projType.equalsIgnoreCase("beam"))
    {
      shootBeam(p, spell, damage);
      return;
    }
    proj.setMetadata("projectileMetadata", new FixedMetadataValue(this, spell));
    proj.setMetadata("projectilePlayerMetadata", new FixedMetadataValue(this, p.getName()));
    if (getConfig().getString("spells." + spell + ".trailParticalEffect") != null) {
      makeTrail(proj, getConfig().getString("spells." + spell + ".trailParticalEffect"));
    }
    if ((!drop) && 
      (getConfig().getString("spells." + spell + ".randomRadius") != null))
    {
      Location l = proj.getLocation().clone();
      Vector v = proj.getVelocity();
      double radius = getConfig().getDouble("spells." + spell + ".randomRadius");
      ArrayList<Location> ll = getArea(l, radius, 0.2D);
      if (radius > 0.0D)
      {
        int index = new Random().nextInt(ll.size());
        proj.teleport((Location)ll.get(index));
        proj.setVelocity(v);
      }
    }
    double speed = getConfig().getDouble("spells." + spell + ".speed");
    if (drop)
    {
      if (speed != 0.0D)
      {
        proj.setVelocity(new Vector(0.0D, speed, 0.0D));
        if (((proj instanceof Fireball)) || ((proj instanceof WitherSkull)))
        {
          int height = getConfig().getInt("spells." + spell + ".height");
          moveToward(proj, new Location(sky.getWorld(), sky.getX(), sky.getY() - height, sky.getZ()), speed);
        }
      }
    }
    else if (speed != 0.0D)
    {
      proj.setVelocity(p.getLocation().getDirection().multiply(speed));
      if (getConfig().getBoolean("spells." + spell + ".homing"))
      {
        Entity t = getTarget(p);
        if (t != null) {
          moveToward(proj, t, speed);
        }
      }
      else if (((proj instanceof Fireball)) || ((proj instanceof WitherSkull)))
      {
        moveToward(proj, p.getTargetBlock((Set<Material>)null, 100).getLocation(), speed);
      }
    }
    int dig = getConfig().getInt("spells." + spell + ".effects.dig");
    int power = getConfig().getInt("spells." + spell + ".splash.effects.digPower");
    entityCauseDamage(p, proj, damage, dig + ":" + power, spell, getPotions("spells." + spell + ".effects.potionEffects"), getPotions("spells." + spell + ".effects.clearPotionEffects"), new ArrayList(Arrays.asList(new Player[] { p })));
    if (life > 0) {
      endLife(p.getUniqueId(), proj, life);
    }
  }
  
  public void moveToward(final Entity e, final Location to, final double speed)
  {
    if (e.isDead()) {
      return;
    }
    Vector direction = to.toVector().subtract(e.getLocation().toVector()).normalize();
    e.setVelocity(direction.multiply(speed));
    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
    {
      public void run()
      {
        try
        {
          moveToward(e, to, speed);
        }
        catch (Exception localException) {}
      }
    }, 1L);
  }
  
  public void moveToward(final Entity e, final Entity to, final double speed)
  {
    if (e.isDead()) {
      return;
    }
    Vector direction = to.getLocation().toVector().subtract(e.getLocation().toVector()).normalize();
    e.setVelocity(direction.multiply(speed));
    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
    {
      public void run()
      {
        try
        {
          moveToward(e, to, speed);
        }
        catch (Exception localException) {}
      }
    }, 1L);
  }
  
  private void shootBeam(Player p, String projName, int damage)
  {
    String effect = getConfig().getString("spells." + projName + ".trailParticalEffect");
    ArrayList<PotionEffect> effectList = getPotions("spells." + projName + ".effects.potionEffects");
    Location eyeLoc = p.getEyeLocation();
    double px = eyeLoc.getX();
    double py = eyeLoc.getY();
    double pz = eyeLoc.getZ();
    double yaw = Math.toRadians(eyeLoc.getYaw() + 90.0F);
    double pitch = Math.toRadians(eyeLoc.getPitch() + 90.0F);
    double x = Math.sin(pitch) * Math.cos(yaw);
    double y = Math.sin(pitch) * Math.sin(yaw);
    double z = Math.cos(pitch);
    if (getTarget(p) != null)
    {
      Entity de = getTarget(p);
      ((LivingEntity)de).damage(Math.round(damage));
      if (effectList != null) {
        for (PotionEffect pe : effectList) {
          if (pe.getType().equals(PotionEffectType.ABSORPTION)) {
            ((LivingEntity)de).setFireTicks(pe.getDuration());
          } else {
            ((LivingEntity)de).addPotionEffect(pe);
          }
        }
      }
    }
    Location last = null;
    int range = getConfig().getInt("spells." + projName + ".beamRange");
    
    boolean drop = getConfig().getBoolean("spells." + projName + ".drop");
    int height = getConfig().getInt("spells." + projName + ".height");
    Location sky = p.getTargetBlock((Set<Material>)null, range).getLocation();
    sky.setY(sky.getY() + height);
    
    int dig = getConfig().getInt("spells." + projName + ".splash.effects.dig");
    int power = getConfig().getInt("spells." + projName + ".splash.effects.digPower");
    boolean rep = getConfig().getBoolean("spells." + projName + ".splash.effects.digReplace");
    if (drop)
    {
      for (int i = 0; i <= range; i++)
      {
        Location loc = new Location(p.getWorld(), sky.getX(), sky.getY() - i, sky.getZ());
        
        beamParticals(effect, projName, loc);
        if (getConfig().getString("spells." + projName + ".effects.changeBlock") != null)
        {
          String[] s = getConfig().getString("spells." + projName + ".effects.changeBlock").split(":");
          changeBlocks(p, s, loc);
        }
        if (dig > 0) {
          dig(p, rep, loc, dig, power);
        }
        if ((loc.getBlock().getType() != Material.AIR) || (i >= range))
        {
          if (last != null) {
            loc = last;
          }
          splashLocation(loc.getBlock().getLocation(), projName, p);
          break;
        }
        last = loc.clone();
      }
      return;
    }
    for (int i = 1; i <= range; i++)
    {
      Location loc = new Location(p.getWorld(), px + i * x, py + i * z, pz + i * y);
      
      beamParticals(effect, projName, loc);
      if (getConfig().getString("spells." + projName + "effects.changeBlock") != null)
      {
        String[] s = getConfig().getString("spells." + projName + ".effects.changeBlock").split(":");
        changeBlocks(p, s, loc);
      }
      if (dig > 0) {
        dig(p, rep, loc, dig, power);
      }
      if ((loc.getBlock().getType() != Material.AIR) || (i >= range))
      {
        if (last != null) {
          loc = last;
        }
        splashLocation(loc.getBlock().getLocation(), projName, p);
        break;
      }
      last = loc.clone();
    }
  }
  
  private void beamParticals(String effect, String spell, Location loc)
  {
    if (getConfig().getString("spells." + spell + ".trailParticalEffect") != null)
    {
      String[] split = effect.split(":");
      int speed = Integer.parseInt(split[1]);
      int amount = Integer.parseInt(split[2]);
      double r = Double.parseDouble(split[3]);
      displayParticle(split[0], loc.getWorld(), loc.getX(), loc.getY(), loc.getZ(), r, speed, amount);
    }
  }
  
  public void displayParticle2(String effect, Location l, double radius, int speed, int amount){
	  l.getWorld().playEffect(l, Effect.getByName(effect), amount);
  }
  
  public void displayParticle(String effect, Location l, double radius, int speed, int amount){
  	displayParticle(effect, l.getWorld(), l.getX(), l.getY(), l.getZ(), radius, speed, amount);
  }
  
  private void displayParticle(String effect, World w, double x, double y, double z, double radius, int speed, int amount){
		Location l = new Location(w, x, y, z);
		//System.out.println("V: " + getServer().getVersion());
		if(Bukkit.getVersion().contains("1.12")){
			if(radius == 0){
				ParticleEffects_1_12.sendToLocation(ParticleEffects_1_12.valueOf(effect), l, 0, 0, 0, speed, amount);
			}else{
				ArrayList<Location> ll = getArea(l, radius, 0.2);
				for(int i = 0; i < amount; i++){
			        int index = new Random().nextInt(ll.size());
			        ParticleEffects_1_12.sendToLocation(ParticleEffects_1_12.valueOf(effect), ll.get(index), 0, 0, 0, speed, 1);
			        ll.remove(index);
				}
			}
		}else if(Bukkit.getVersion().contains("1.11")){
			if(radius == 0){
				ParticleEffects_1_11.sendToLocation(ParticleEffects_1_11.valueOf(effect), l, 0, 0, 0, speed, amount);
			}else{
				ArrayList<Location> ll = getArea(l, radius, 0.2);
				for(int i = 0; i < amount; i++){
			        int index = new Random().nextInt(ll.size());
			        ParticleEffects_1_11.sendToLocation(ParticleEffects_1_11.valueOf(effect), ll.get(index), 0, 0, 0, speed, 1);
			        ll.remove(index);
				}
			}
		}else if(Bukkit.getVersion().contains("1.10")){
			if(radius == 0){
				ParticleEffects_1_10.sendToLocation(ParticleEffects_1_10.valueOf(effect), l, 0, 0, 0, speed, amount);
			}else{
				ArrayList<Location> ll = getArea(l, radius, 0.2);
				for(int i = 0; i < amount; i++){
			        int index = new Random().nextInt(ll.size());
			        ParticleEffects_1_10.sendToLocation(ParticleEffects_1_10.valueOf(effect), ll.get(index), 0, 0, 0, speed, 1);
			        ll.remove(index);
				}
			}
		}else if(Bukkit.getVersion().contains("1.9.4")){
			if(radius == 0){
				ParticleEffects_1_9_4.sendToLocation(ParticleEffects_1_9_4.valueOf(effect), l, 0, 0, 0, speed, amount);
			}else{
				ArrayList<Location> ll = getArea(l, radius, 0.2);
				for(int i = 0; i < amount; i++){
			        int index = new Random().nextInt(ll.size());
			        ParticleEffects_1_9_4.sendToLocation(ParticleEffects_1_9_4.valueOf(effect), ll.get(index), 0, 0, 0, speed, 1);
			        ll.remove(index);
				}
			}
		}else if(getServer().getVersion().contains("1.8.6") || getServer().getVersion().contains("1.8.7") || getServer().getVersion().contains("1.8.8") || getServer().getVersion().contains("1.8.9")){
			if(radius == 0){
				ParticleEffects_1_8_6.sendToLocation(ParticleEffects_1_8_6.valueOf(effect), l, 0, 0, 0, speed, amount);
			}else{
				ArrayList<Location> ll = getArea(l, radius, 0.2);
				for(int i = 0; i < amount; i++){
			        int index = new Random().nextInt(ll.size());
			        ParticleEffects_1_8_6.sendToLocation(ParticleEffects_1_8_6.valueOf(effect), ll.get(index), 0, 0, 0, speed, 1);
			        ll.remove(index);
				}
			}
}
  }
  
  private ArrayList<Location> getArea(Location l, double r, double t){
    ArrayList<Location> ll = new ArrayList();
    for (double x = l.getX() - r; x < l.getX() + r; x += t) {
      for (double y = l.getY() - r; y < l.getY() + r; y += t) {
        for (double z = l.getZ() - r; z < l.getZ() + r; z += t) {
          ll.add(new Location(l.getWorld(), x, y, z));
        }
      }
    }
    return ll;
  }
  
  private ArrayList<Location> getFlatArea(Location l, double r, double t)
  {
    ArrayList<Location> ll = new ArrayList();
    for (double x = l.getX() - r; x < l.getX() + r; x += t) {
      for (double z = l.getZ() - r; z < l.getZ() + r; z += t) {
        ll.add(new Location(l.getWorld(), x, l.getY(), z));
      }
    }
    return ll;
  }
  
  private void castCool(final Player p)
  {
    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
    {
      public void run()
      {
        try
        {
          WizardlyMagic.this.castCool.remove(p.getName());
        }
        catch (Exception localException) {}
      }
    }, 2L);
  }
  
	public static Entity getTarget(final Player player) {
		 
        BlockIterator iterator = new BlockIterator(player.getWorld(), player
                .getLocation().toVector(), player.getEyeLocation()
                .getDirection(), 0, 100);
        Entity target = null;
        while (iterator.hasNext()) {
            Block item = iterator.next();
            for (Entity entity : player.getNearbyEntities(100, 100, 100)) {
            	if((entity instanceof LivingEntity) && (!(entity.getType().equals(EntityType.BAT)))){
	                int acc = 2;
	                for (int x = -acc; x < acc; x++)
	                    for (int z = -acc; z < acc; z++)
	                        for (int y = -acc; y < acc; y++)
	                            if (entity.getLocation().getBlock()
	                                    .getRelative(x, y, z).equals(item)) {
	                                return target = entity;
	                            }
            	}
            }
        }
        return target;
    }
  
  private void splashEntity(Entity e)
  {
    if ((e.isDead()) && (!this.projList.contains(e.getUniqueId()))) {
      return;
    }
    String projName = null;
    for (MetadataValue v : e.getMetadata("projectileMetadata")) {
      projName = v.asString();
    }
    Player p = null;
    for (MetadataValue v : e.getMetadata("projectilePlayerMetadata")) {
      p = getServer().getPlayer(v.asString());
    }
    splashLocation(e.getLocation(), projName, p);
    
    this.projList.remove(e.getUniqueId());
    e.remove();
  }

	private void splashLocation(Location l, String projName, final Player player){
		//System.out.println("Splash1");
		try{
			int radius = getConfig().getInt("spells." + projName + ".splash.radius");
			//Sound
			if(getConfig().getString("spells." + projName + ".splash.sound") != null)
				l.getWorld().playSound(l, Sound.valueOf(getConfig().getString("spells." + projName + ".splash.sound")), 1, 1);
			//Explode
			if((getConfig().getBoolean("spells." + projName + ".splash.explode") == true))
				l.getWorld().createExplosion(l, 2);
			//Lightning
			if((getConfig().getBoolean("spells." + projName + ".splash.lightning") == true))
				l.getWorld().strikeLightning(l);
			//Knock Back
			int kb = getConfig().getInt("spells." + projName + ".splash.knockBack");
			//TP
			if((getConfig().getString("spells." + projName + ".splash.tp") != null) && (getConfig().getBoolean("spells." + projName + ".splash.tp") == true))
				player.teleport(l);
			//Mark
			if((getConfig().getBoolean("spells." + projName + ".splash.mark") == true))
				setLocation("players." + player.getUniqueId().toString() + ".mark", l, saveFile);
			//Change Blocks
			if(getConfig().getString("spells." + projName + ".splash.changeBlock") != null){
				String[] s = getConfig().getString("spells." + projName + ".splash.changeBlock").split(":");
				List<Location> bls = circle (l, radius, 0, false, true, 0);
				for(final Location bl : bls)
					if(!bl.getBlock().getType().equals(Material.AIR))
						changeBlocks(player, s, bl);
			}
			//Gravity Blocks
			if(getConfig().getBoolean("spells." + projName + ".splash.gravityBlocks")){
				List<Location> bls = circle (l, radius, 0, false, true, 0);
				for(Location bl : bls){
					Block b = bl.getBlock();
					if(!b.getType().equals(Material.BEDROCK)){
						b.getWorld().spawnFallingBlock(bl, b.getTypeId(), b.getData());
						b.setType(Material.AIR);
					}
				}
			}
			//Effects
			//System.out.println("Effect: " + radius);
			//for(Entity ent : e.getNearbyEntities(radius, radius, radius)){
			for(Entity ent : getNearbyEntities(l, radius, null)){
				//Velocity
				if(getConfig().getString("spells." + projName + ".splash.velocity") != null){
					String[] s = getConfig().getString("spells." + projName + ".splash.velocity").split(":");
					if(s[0].equals("forward")){
						((LivingEntity)ent).setVelocity(((LivingEntity)ent).getLocation().getDirection().multiply(Double.parseDouble(s[1])));
					}else{
						double x;
						double y;
						double z;
						if(s[0].contains("r")){
							String[] i = s[0].replace("r", "").split(",");
							x = rand(Double.parseDouble(i[0]), Double.parseDouble(i[1]));
						}else
							x = Double.parseDouble(s[0]);
						if(s[1].contains("r")){
							String[] i = s[1].replace("r", "").split(",");
							y = rand(Double.parseDouble(i[0]), Double.parseDouble(i[1]));
						}else
							y = Double.parseDouble(s[1]);
						if(s[2].contains("r")){
							String[] i = s[2].replace("r", "").split(",");
							z = rand(Double.parseDouble(i[0]), Double.parseDouble(i[1]));
						}else
							z = Double.parseDouble(s[2]);
//						//System.out.println("------------");
//						//System.out.println("x: " + x);
//						//System.out.println("y: " + y);
//						//System.out.println("z: " + z);
						ent.setVelocity(new Vector(x, y, z));
					}
				}
				if(ent instanceof LivingEntity){
					//Recall
					if((getConfig().getBoolean("spells." + projName + ".splash.recall") == true))
						try{ent.teleport(getLocation("players." + player.getUniqueId().toString() + ".mark", saveFile));}catch(Exception x){}
					//System.out.println("Found: " + ent);
					if(kb > 0)
						((LivingEntity)ent).setVelocity(l.toVector().subtract(l.toVector()).multiply(kb));
					if(getConfig().getString("spells." + projName + ".splash.effects.damage") != null)
						((LivingEntity)ent).damage((int)Math.round(getConfig().getInt("spells." + projName + ".splash.effects.damage")));
					if(getConfig().getBoolean("spells." + projName + ".splash.effects.disarm")){
						if(ent instanceof Player){
							try{
								Player p = (Player) ent;
								p.getWorld().dropItemNaturally(p.getLocation(), p.getItemInHand());
								p.setItemInHand(null);
								p.updateInventory();
							}catch(Exception x){}
						}else
							try{
								EntityEquipment ee = ((LivingEntity) ent).getEquipment();
								ent.getWorld().dropItemNaturally(ent.getLocation(), ee.getItemInHand());
								ee.setItemInHand(null);
							}catch(Exception x){}
					}
					if(getConfig().getList("spells." + projName + ".splash.effects.potionEffects") != null)
						for(PotionEffect pe : getPotions("spells." + projName + ".splash.effects.potionEffects"))
							if(pe.getType().equals(PotionEffectType.ABSORPTION))
								((LivingEntity)ent).setFireTicks(pe.getDuration());
							else
								((LivingEntity)ent).addPotionEffect(pe);
				}
			}
			//command
//			if(getConfig().getString("spells." + projName + ".splash.command") != null){
//				String cmd = getConfig().getString("spells." + projName + ".splash.command");
//				runCommand(cmd,l,player,getConfig().getBoolean("spells." + projName + ".splash.playerCommand") );
//			}
			//command
			if(getConfig().getList("spells." + projName + ".splash.commands") != null){
				for(String cmd : (ArrayList<String>)getConfig().getList("spells." + projName + ".splash.commands"))
					runCommand(cmd,l,player,getConfig().getBoolean("spells." + projName + ".splash.playerCommand") );
			}
			//FireWork
			if(getConfig().getString("spells." + projName + ".splash.fireWork") != null){
				//System.out.println("FireWork");
				String[] s = getConfig().getString("spells." + projName + ".splash.fireWork").split(":");
				int r;
				int g;
				int b;
				if(s[0].contains("r")){
					r = rand(0, 255);
				}else
					r = Integer.parseInt(s[0]);
				if(s[1].contains("r")){
					g = rand(0, 255);
				}else
					g = Integer.parseInt(s[1]);
				if(s[2].contains("r")){
					b = rand(0, 255);
				}else
					b = Integer.parseInt(s[2]);
				launchFirework(l, getConfig().getInt("spells." + projName + ".splash.fireWorkLife"), getColor(r, g, b));
			}
			//Dig
			int dig = getConfig().getInt("spells." + projName + ".splash.effects.dig");
			int power = getConfig().getInt("spells." + projName + ".splash.effects.digPower");
			boolean rep = getConfig().getBoolean("spells." + projName + ".splash.effects.digReplace");
			if(dig > 0)
				dig(player, rep, l, dig, power);
			//Particals
			if(getConfig().getString("spells." + projName + ".splash.particalEffect") != null){
				String[] split = getConfig().getString("spells." + projName + ".splash.particalEffect").split(":");
				int speed = Integer.parseInt(split[1]);
				int amount = Integer.parseInt(split[2]);
				double r = Double.parseDouble(split[3]);
				Location loc = l.clone();
				displayParticle(split[0], loc.getWorld(), loc.getX(), loc.getY(), loc.getZ(), r, speed, amount);
				//ParticleEffects.sendToLocation(ParticleEffects.valueOf(split[0]), e.getLocation(), 0, 0, 0, Integer.parseInt(split[1]), Integer.parseInt(split[2]));
			}
		}catch(Exception x){x.printStackTrace();}
		//System.out.println("Splash3");
	}
	
	private void runCommand(String cmd, Location l, Player p, boolean psc){
		cmd = cmd.replace("<Player>", p.getName()).replace("<x>", l.getX()+"").replace("<y>", l.getY()+"").replace("<z>", l.getZ()+"").replace("&", "§");
		if(psc){
			boolean io = p.isOp();
			p.setOp(true);
			Bukkit.getServer().dispatchCommand(p, cmd);
			if(!io)
				p.setOp(false);
		}else
			Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), cmd);
	}
  
//  private void splashLocation(Location l, String projName, Player player)
//  {
//    try
//    {
//      int radius = getConfig().getInt("spells." + projName + ".splash.radius");
//      if (getConfig().getString("spells." + projName + ".splash.sound") != null) {
//        l.getWorld().playSound(l, Sound.valueOf(getConfig().getString("spells." + projName + ".splash.sound")), 1.0F, 1.0F);
//      }
//      if (getConfig().getBoolean("spells." + projName + ".splash.explode")) {
//        l.getWorld().createExplosion(l, 2.0F);
//      }
//      if (getConfig().getBoolean("spells." + projName + ".splash.lightning")) {
//        l.getWorld().strikeLightning(l);
//      }
//      int kb = getConfig().getInt("spells." + projName + ".splash.knockBack");
//      if ((getConfig().getString("spells." + projName + ".splash.tp") != null) && (getConfig().getBoolean("spells." + projName + ".splash.tp"))) {
//        player.teleport(l);
//      }
//      if (getConfig().getBoolean("spells." + projName + ".splash.mark")) {
//        setLocation("players." + player.getUniqueId().toString() + ".mark", l, this.saveFile);
//      }
//      Location bl;
//      if (getConfig().getString("spells." + projName + ".splash.changeBlock") != null)
//      {
//        String[] s = getConfig().getString("spells." + projName + ".splash.changeBlock").split(":");
//        List<Location> bls = circle(l, Integer.valueOf(radius), Integer.valueOf(0), Boolean.valueOf(false), Boolean.valueOf(true), 0);
//        for (Iterator localIterator1 = bls.iterator(); localIterator1.hasNext();)
//        {
//          bl = (Location)localIterator1.next();
//          if (!bl.getBlock().getType().equals(Material.AIR)) {
//            changeBlocks(player, s, bl);
//          }
//        }
//      }
//      Location bl;
//      if (getConfig().getBoolean("spells." + projName + ".splash.gravityBlocks"))
//      {
//        List<Location> bls = circle(l, Integer.valueOf(radius), Integer.valueOf(0), Boolean.valueOf(false), Boolean.valueOf(true), 0);
//        for (bl = bls.iterator(); bl.hasNext();)
//        {
//          bl = (Location)bl.next();
//          Block b = bl.getBlock();
//          if (!b.getType().equals(Material.BEDROCK))
//          {
//            b.getWorld().spawnFallingBlock(bl, b.getTypeId(), b.getData());
//            b.setType(Material.AIR);
//          }
//        }
//      }
//      for (Entity ent : getNearbyEntities(l, radius, null))
//      {
//        double x;
//        if (getConfig().getString("spells." + projName + ".splash.velocity") != null)
//        {
//          String[] s = getConfig().getString("spells." + projName + ".splash.velocity").split(":");
//          if (s[0].equals("forward"))
//          {
//            ((LivingEntity)ent).setVelocity(((LivingEntity)ent).getLocation().getDirection().multiply(Double.parseDouble(s[1])));
//          }
//          else
//          {
//            double x;
//            if (s[0].contains("r"))
//            {
//              String[] i = s[0].replace("r", "").split(",");
//              x = rand(Double.parseDouble(i[0]), Double.parseDouble(i[1]));
//            }
//            else
//            {
//              x = Double.parseDouble(s[0]);
//            }
//            double y;
//            double y;
//            if (s[1].contains("r"))
//            {
//              String[] i = s[1].replace("r", "").split(",");
//              y = rand(Double.parseDouble(i[0]), Double.parseDouble(i[1]));
//            }
//            else
//            {
//              y = Double.parseDouble(s[1]);
//            }
//            double z;
//            double z;
//            if (s[2].contains("r"))
//            {
//              String[] i = s[2].replace("r", "").split(",");
//              z = rand(Double.parseDouble(i[0]), Double.parseDouble(i[1]));
//            }
//            else
//            {
//              z = Double.parseDouble(s[2]);
//            }
//            ent.setVelocity(new Vector(x, y, z));
//          }
//        }
//        if ((ent instanceof LivingEntity))
//        {
//          if (getConfig().getBoolean("spells." + projName + ".splash.recall")) {
//            try
//            {
//              ent.teleport(getLocation("players." + player.getUniqueId().toString() + ".mark", this.saveFile));
//            }
//            catch (Exception localException1) {}
//          }
//          if (kb > 0) {
//            ((LivingEntity)ent).setVelocity(l.toVector().subtract(l.toVector()).multiply(kb));
//          }
//          if (getConfig().getString("spells." + projName + ".splash.effects.damage") != null) {
//            ((LivingEntity)ent).damage(Math.round(getConfig().getInt("spells." + projName + ".splash.effects.damage")));
//          }
//          if (getConfig().getBoolean("spells." + projName + ".splash.effects.disarm")) {
//            if ((ent instanceof Player)) {
//              try
//              {
//                Player p = (Player)ent;
//                p.getWorld().dropItemNaturally(p.getLocation(), p.getItemInHand());
//                p.setItemInHand(null);
//                p.updateInventory();
//              }
//              catch (Exception localException2) {}
//            } else {
//              try
//              {
//                EntityEquipment ee = ((LivingEntity)ent).getEquipment();
//                ent.getWorld().dropItemNaturally(ent.getLocation(), ee.getItemInHand());
//                ee.setItemInHand(null);
//              }
//              catch (Exception localException3) {}
//            }
//          }
//          if (getConfig().getList("spells." + projName + ".splash.effects.potionEffects") != null) {
//            for (PotionEffect pe : getPotions("spells." + projName + ".splash.effects.potionEffects")) {
//              if (pe.getType().equals(PotionEffectType.ABSORPTION)) {
//                ((LivingEntity)ent).setFireTicks(pe.getDuration());
//              } else {
//                ((LivingEntity)ent).addPotionEffect(pe);
//              }
//            }
//          }
//        }
//      }
//      if (getConfig().getString("spells." + projName + ".splash.fireWork") != null)
//      {
//        String[] s = getConfig().getString("spells." + projName + ".splash.fireWork").split(":");
//        int r;
//        int r;
//        if (s[0].contains("r")) {
//          r = rand(0, 255);
//        } else {
//          r = Integer.parseInt(s[0]);
//        }
//        int g;
//        int g;
//        if (s[1].contains("r")) {
//          g = rand(0, 255);
//        } else {
//          g = Integer.parseInt(s[1]);
//        }
//        int b;
//        int b;
//        if (s[2].contains("r")) {
//          b = rand(0, 255);
//        } else {
//          b = Integer.parseInt(s[2]);
//        }
//        launchFirework(l, getConfig().getInt("spells." + projName + ".splash.fireWorkLife"), getColor(r, g, b));
//      }
//      int dig = getConfig().getInt("spells." + projName + ".splash.effects.dig");
//      int power = getConfig().getInt("spells." + projName + ".splash.effects.digPower");
//      boolean rep = getConfig().getBoolean("spells." + projName + ".splash.effects.digReplace");
//      if (dig > 0) {
//        dig(player, rep, l, dig, power);
//      }
//      if (getConfig().getString("spells." + projName + ".splash.particalEffect") != null)
//      {
//        String[] split = getConfig().getString("spells." + projName + ".splash.particalEffect").split(":");
//        int speed = Integer.parseInt(split[1]);
//        int amount = Integer.parseInt(split[2]);
//        double r = Double.parseDouble(split[3]);
//        Location loc = l.clone();
//        displayParticle(split[0], loc.getWorld(), loc.getX(), loc.getY(), loc.getZ(), r, speed, amount);
//      }
//    }
//    catch (Exception x)
//    {
//      x.printStackTrace();
//    }
//  }
  
  private void changeBlocks(final Player p, String[] s, final Location l)
  {
    p.sendBlockChange(l, Integer.parseInt(s[0]), (byte)Integer.parseInt(s[1]));
    
    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
    {
      public void run()
      {
        try
        {
          p.sendBlockChange(l, l.getBlock().getType(), l.getBlock().getData());
        }
        catch (Exception localException) {}
      }
    }, Integer.parseInt(s[2]) * 20);
  }
  
  private static List<Location> circle(Location loc, Integer r, Integer h, Boolean hollow, Boolean sphere, int plus_y)
  {
    List<Location> circleblocks = new ArrayList<Location>();
    int cx = loc.getBlockX();
    int cy = loc.getBlockY();
    int cz = loc.getBlockZ();
    for (int x = cx - r.intValue(); x <= cx + r.intValue(); x++) {
      for (int z = cz - r.intValue(); z <= cz + r.intValue(); z++) {
        for (int y = sphere.booleanValue() ? cy - r.intValue() : cy; y < (sphere.booleanValue() ? cy + r.intValue() : cy + h.intValue()); y++)
        {
          double dist = (cx - x) * (cx - x) + (cz - z) * (cz - z) + (sphere.booleanValue() ? (cy - y) * (cy - y) : 0);
          if ((dist < r.intValue() * r.intValue()) && ((!hollow.booleanValue()) || (dist >= (r.intValue() - 1) * (r.intValue() - 1))))
          {
            Location l = new Location(loc.getWorld(), x, y + plus_y, z);
            circleblocks.add(l);
          }
        }
      }
    }
    return circleblocks;
  }
  
  private void cool(final UUID id, String projName)
  {
    this.coolMap.put(id, projName);
    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
    {
      public void run()
      {
        WizardlyMagic.this.coolMap.remove(id);
      }
    }, getConfig().getInt("spells." + projName + ".cooldown"));
  }
  
  private void makeTrail(final Entity e, final String effect)
  {
    if (!isAlive(e)) {
      return;
    }
    String[] split = effect.split(":");
    int speed = Integer.parseInt(split[1]);
    int amount = Integer.parseInt(split[2]);
    double r = Double.parseDouble(split[3]);
    Location loc = e.getLocation();
    displayParticle(split[0], loc.getWorld(), loc.getX(), loc.getY(), loc.getZ(), r, speed, amount);
    
    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
    {
      public void run()
      {
        WizardlyMagic.this.makeTrail(e, effect);
      }
    }, 1L);
  }
  
  private boolean isAlive(Entity e){
    if ((e instanceof FallingBlock)){
      FallingBlock fb = (FallingBlock)e;
      if (fb.isOnGround()) {
        return false;
      }
    }
    if (e.isDead()) {
      return false;
    }
    return true;
  }
  
  private ArrayList<PotionEffect> getPotions(String path){
	  try{
		  ArrayList<PotionEffect> pList = new ArrayList();
		  for(String p : (ArrayList<String>)getConfig().getList(path)){
			  String[] split = p.split(":");
			  PotionEffect pe;
			  if(split[0].equalsIgnoreCase("fire")) {
				  pe = new PotionEffect(PotionEffectType.ABSORPTION, Integer.parseInt(split[2]), Integer.parseInt(split[1]));
			  }else
				  pe = new PotionEffect(PotionEffectType.getByName(split[0]), Integer.parseInt(split[2]), Integer.parseInt(split[1]));
			  pList.add(pe);
		  }
		  return pList;
	  }catch (Exception x) {}
	  return null;
  }
  
  private void endLife(UUID id, final Entity e, int l)
  {
    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
    {
      public void run()
      {
        WizardlyMagic.this.splashEntity(e);
      }
    }, l);
  }
  
  private void entityCauseDamage(final Player p, final Entity e, final int damage, final String dig, final String spell, final ArrayList<PotionEffect> effectList, final ArrayList<PotionEffect> reffectList, ArrayList<Entity> dList)
  {
    if (!isAlive(e))
    {
      splashLocation(e.getLocation(), spell, p);
      return;
    }
    for (Entity de : e.getNearbyEntities(1.0D, 1.0D, 1.0D)) {
      if (((de instanceof LivingEntity)) && (!dList.contains(de)))
      {
        effect((LivingEntity)de, damage, effectList, reffectList);
        dList.add(de);
      }
    }
    if (getConfig().getString("spells." + spell + ".effects.changeBlock") != null)
    {
      String[] s = getConfig().getString("spells." + spell + ".effects.changeBlock").split(":");
      changeBlocks(p, s, e.getLocation());
    }
    String[] s = dig.split(":");
    if (!s[0].equals("0"))
    {
      boolean rep = getConfig().getBoolean("spells." + spell + ".effects.digReplace");
      dig(p, rep, e.getLocation(), Integer.parseInt(s[0]), Integer.parseInt(s[1]));
    }
    final ArrayList<Entity> tList = (ArrayList)dList.clone();
    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
      public void run(){
       entityCauseDamage(p, e, damage, dig, spell, effectList, reffectList, tList);
      }
    }, 2L);
  }
  
  private void launchFirework(Location l, int life, Color color){
    Firework fw = (Firework)l.getWorld().spawn(l, Firework.class);
    FireworkMeta meta = fw.getFireworkMeta();
    meta.addEffect(FireworkEffect.builder().withColor(color).with(FireworkEffect.Type.BALL_LARGE).build());
    fw.setFireworkMeta(meta);
    
    detonate(fw, life);
  }
  
  private Color getColor(int r, int g, int b){
    ItemStack tmpCol = new ItemStack(Material.LEATHER_HELMET, 1);
    LeatherArmorMeta tmpCol2 = (LeatherArmorMeta)tmpCol.getItemMeta();
    tmpCol2.setColor(Color.fromRGB(r, g, b));
    return tmpCol2.getColor();
  }
  
	public void detonate(final Firework fw, int life){
	    //Wait life and Detontae
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			public void run() {
				try{
					fw.detonate();
				}catch(Exception e){}
			}
		}, (life));
	}
  
  private void dig(Player p, boolean rep, Location l, int s, int power){
    ItemStack rs = null;
    if (rep) {
      rs = getBlocksBeside(p);
    }
    Material m = l.getBlock().getType();
    if (s > 0) {
      if (s == 1) {
        mineBlock(l.getBlock(), power, rep, rs, p);
      } else {
        for (double x = l.getX() - s / 2; x <= l.getX() + s / 2; x += 1.0D) {
          for (double y = l.getY() - s / 2; y <= l.getY() + s / 2; y += 1.0D) {
            for (double z = l.getZ() - s / 2; z <= l.getZ() + s / 2; z += 1.0D) {
              if (rep)
              {
                Block b = new Location(l.getWorld(), x, y, z).getBlock();
                if ((rs == null) || (rs.getType().equals(Material.AIR))) {
                  rs = getBlocksBeside(p);
                }
                if ((b.getType().equals(m)) && 
                  (mineBlock(b, power, rep, rs, p))) {
                  rs = null;
                }
              }
              else
              {
                mineBlock(new Location(l.getWorld(), x, y, z).getBlock(), power, rep, rs, p);
              }
            }
          }
        }
      }
    }
    p.updateInventory();
  }
  
  private boolean mineBlock(Block b, int power, boolean rep, ItemStack rs, Player p)
  {
    Lockette loc = (Lockette)Bukkit.getServer().getPluginManager().getPlugin("Lockette");
    if (loc != null) {
      if ((!loc.canBuild(p, b)) || (!Lockette.isOwner(b, p))) {
        return false;
      }
    }
    LWCPlugin lwc = (LWCPlugin)Bukkit.getServer().getPluginManager().getPlugin("LWC");
    if (lwc != null)
    {
      Protection prot = lwc.getLWC().findProtection(b.getLocation());
      if ((prot != null) && (prot.getOwner() != null) && (!prot.getOwner().equals(p)) && (!lwc.getLWC().canAccessProtection(p, b))) {
        return false;
      }
    }
    if ((!b.getType().equals(Material.AIR)) && (!b.getType().equals(Material.BEDROCK)))
    {
      int id = b.getTypeId();
      if (((id == 14) || (id == 56) || (id == 73) || (id == 74)) && 
        (power < 1)) {
        return false;
      }
      if ((id == 49) && (power < 2)) {
        return false;
      }
      if (rep){
    	  //Relace Blocks
    	  if (rs != null && (!b.getType().equals(rs.getType()))){
    		  for (ItemStack i : b.getDrops())
    			  p.getInventory().addItem(i);
    		  b.setType(rs.getType());
	          return removeItem(p, rs, 1);
	      }
      }else{
//    	  for (ItemStack i : b.getDrops()) {
//    		  b.getWorld().dropItem(b.getLocation(), i);
//    	  }
//    	  b.setType(Material.AIR);
    	  b.breakNaturally();
    	  //System.out.println("Break Nat");
      }
    }
    return false;
  }
  
  private ItemStack getBlocksBeside(Player p){
    if ((p.getInventory().getItem(p.getInventory().getHeldItemSlot() - 1) != null) || (p.getInventory().getItem(p.getInventory().getHeldItemSlot() + 1) != null)) {
      try
      {
        if (p.getInventory().getItem(p.getInventory().getHeldItemSlot() - 1).getTypeId() < 256) {
          return p.getInventory().getItem(p.getInventory().getHeldItemSlot() - 1);
        }
        if (p.getInventory().getItem(p.getInventory().getHeldItemSlot() + 1).getTypeId() < 256) {
          return p.getInventory().getItem(p.getInventory().getHeldItemSlot() + 1);
        }
      }
      catch (Exception localException) {}
    }
    return null;
  }
  
  private boolean removeItem(Player p, ItemStack s, int a){
	  //System.out.println("Remove: " + s.getType());
	  int amount = s.getAmount() - a;
	  if (amount <= 0){
		  p.getInventory().remove(s);
		  return true;
	  }
	  s.setAmount(amount);
	  return false;
  }
  
  private void effect(LivingEntity e, int damage, ArrayList<PotionEffect> effectList, ArrayList<PotionEffect> reffectList)
  {
    if (damage > 0) {
      e.damage(Math.round(damage));
    }
    if (effectList != null) {
      for (PotionEffect pe : effectList) {
        if (pe.getType().equals(PotionEffectType.ABSORPTION)) {
          e.setFireTicks(pe.getDuration());
        } else {
          e.addPotionEffect(pe);
        }
      }
    }
    if (reffectList != null) {
      for (PotionEffect pe : reffectList) {
        if (pe.getType().equals(PotionEffectType.ABSORPTION)) {
          e.setFireTicks(0);
        } else {
          e.removePotionEffect(pe.getType());
        }
      }
    }
  }
  
  private ItemStack getItem(String setItemString, String name, int amount, List<String> loreList){
	  ItemStack item = null;
	  if (setItemString.contains(":")){
		  String[] split = setItemString.split(":");
		  int setItem = Integer.parseInt(split[0]);
		  int subtype = Integer.parseInt(split[1]);
		  item = new ItemStack(setItem, amount, (short)subtype);
	  }else{
		  int setItem = Integer.parseInt(setItemString);
		  item = new ItemStack(setItem, amount);
	  }
	  ItemMeta m = item.getItemMeta();
	  if(name != null)
		  m.setDisplayName(name);
	  if(loreList != null)
		  m.setLore(loreList);
	  item.setItemMeta(m);
	  return item;
  }
  
  private void addRecipes(){
    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
      public void run(){
        try{
			addRecipe(getArcaneScroll(), "arcaneScroll");
			addRecipe(getBookOfKnowledge(null), "bookOfKnowledge");
			addRecipe(getBookOfSpells(), "bookOfSpells");
			addRecipe(getBookOfDrops(), "bookOfDrops");
        }
        catch (Exception e1)
        {
          e1.printStackTrace();
        }
      }
    }, 20L);
  }
  
  private void addRecipe(ItemStack s, String path)
  {
    ArrayList<String> rList = (ArrayList)getConfig().getList(path + ".recipe");
    
    ShapedRecipe firePitRecipe = new ShapedRecipe(s).shape(new String[] { "012", "345", "678" });
    
    String[] f3 = ((String)rList.get(0)).split(", ");
    if (Integer.parseInt(f3[0]) != 0) {
      firePitRecipe.setIngredient('0', Material.getMaterial(Integer.parseInt(f3[0])));
    }
    if (Integer.parseInt(f3[1]) != 0) {
      firePitRecipe.setIngredient('1', Material.getMaterial(Integer.parseInt(f3[1])));
    }
    if (Integer.parseInt(f3[2]) != 0) {
      firePitRecipe.setIngredient('2', Material.getMaterial(Integer.parseInt(f3[2])));
    }
    String[] n3 = ((String)rList.get(1)).split(", ");
    if (Integer.parseInt(n3[0]) != 0) {
      firePitRecipe.setIngredient('3', Material.getMaterial(Integer.parseInt(n3[0])));
    }
    if (Integer.parseInt(n3[1]) != 0) {
      firePitRecipe.setIngredient('4', Material.getMaterial(Integer.parseInt(n3[1])));
    }
    if (Integer.parseInt(n3[2]) != 0) {
      firePitRecipe.setIngredient('5', Material.getMaterial(Integer.parseInt(n3[2])));
    }
    String[] l3 = ((String)rList.get(2)).split(", ");
    if (Integer.parseInt(l3[0]) != 0) {
      firePitRecipe.setIngredient('6', Material.getMaterial(Integer.parseInt(l3[0])));
    }
    if (Integer.parseInt(l3[1]) != 0) {
      firePitRecipe.setIngredient('7', Material.getMaterial(Integer.parseInt(l3[1])));
    }
    if (Integer.parseInt(l3[2]) != 0) {
      firePitRecipe.setIngredient('8', Material.getMaterial(Integer.parseInt(l3[2])));
    }
    getServer().addRecipe(firePitRecipe);
  }
  
  @EventHandler
  public void restrictCrafting(PrepareItemCraftEvent e)
  {
    CraftingInventory ci = e.getInventory();
    try
    {
      if (ci.getResult().getItemMeta().getDisplayName().contains("§5Spell")) {
        ci.setResult(null);
      }
    }
    catch (Exception localException) {}
  }
  
  private void setItem(ItemStack s, String path, FileConfiguration fc){
    if (s != null){
      fc.set(path + ".item", Integer.valueOf(s.getTypeId()));
      fc.set(path + ".amount", Integer.valueOf(s.getAmount()));
      fc.set(path + ".durability", Short.valueOf(s.getDurability()));
      if (s.getItemMeta() != null){
        if (s.getItemMeta().getDisplayName() != null) {
          fc.set(path + ".name", s.getItemMeta().getDisplayName());
        }
        if (s.getItemMeta().getLore() != null) {
          for (int l = 0; l < s.getItemMeta().getLore().size(); l++) {
            if (s.getItemMeta().getLore().get(l) != null) {
              fc.set(path + ".lore" + l, s.getItemMeta().getLore().get(l));
            }
          }
        }
      }
      Enchantment e;
      if (s.getEnchantments() != null) {
        for (Map.Entry<Enchantment, Integer> hm : s.getEnchantments().entrySet()){
          e = (Enchantment)hm.getKey();
          int level = ((Integer)hm.getValue()).intValue();
          for (int ei = 0; ei < 13; ei++) {
            if (fc.getString(path + ".enchantments." + ei) == null){
              fc.set(path + ".enchantments." + ei + ".enchantment", e.getName());
              fc.set(path + ".enchantments." + ei + ".level", Integer.valueOf(level));
              break;
            }
          }
        }
      }
      if (s.getType().equals(Material.ENCHANTED_BOOK)) {
        EnchantmentStorageMeta em = (EnchantmentStorageMeta)s.getItemMeta();
        if (em.getStoredEnchants() != null) {
          for (Object hm : em.getStoredEnchants().entrySet()){
            e = (Enchantment)((Map.Entry)hm).getKey();
            int level = ((Integer)((Map.Entry)hm).getValue()).intValue();
            for (int ei = 0; ei < 13; ei++) {
              if (fc.getString(path + ".enchantments." + ei) == null){
                fc.set(path + ".enchantments." + ei + ".enchantment", e.getName());
                fc.set(path + ".enchantments." + ei + ".level", Integer.valueOf(level));
                break;
              }
            }
          }
        }
      }
      if ((s.getType().equals(Material.WRITTEN_BOOK)) || (s.getType().equals(Material.BOOK_AND_QUILL)))
      {
        BookMeta meta = (BookMeta)s.getItemMeta();
        if (meta.getAuthor() != null) {
          fc.set(path + ".author", meta.getAuthor());
        }
        if (meta.getTitle() != null) {
          fc.set(path + ".title", meta.getTitle());
        }
        int i = 0;
        if (meta.getPages() != null) {
          for (String p : meta.getPages())
          {
            fc.set(path + ".pages." + i, p);
            i++;
          }
        }
      }
      //Banner
      if (s.getType().equals(Material.BANNER)){
    	  BannerMeta b = (BannerMeta)s.getItemMeta();
    	  if(b != null){
	    	  List patList = b.getPatterns();
	    	  if(patList != null && (!patList.isEmpty()))
	    		  fc.set(path + ".patterns", patList);
    	  }
      }
      //Shield
      if(is9())
	      if (s.getType().equals(Material.SHIELD)){
	    	  ItemMeta im = s.getItemMeta();
	    	  BlockStateMeta bmeta = (BlockStateMeta) im;
	    	  Banner b = (Banner) bmeta.getBlockState();
	    	  if(b != null){
	    		  fc.set(path + ".colour", b.getBaseColor().toString());
		    	  List patList = b.getPatterns();
		    	  if(patList != null && (!patList.isEmpty()))
		    		  fc.set(path + ".patterns", patList);
	    	  }
	      }
      //Potions
      if(is9())
	      if(s.getType().equals(Material.POTION) || s.getType().equals(Material.SPLASH_POTION) || s.getType().equals(Material.LINGERING_POTION)){
				PotionMeta pMeta = (PotionMeta) s.getItemMeta();
				org.bukkit.potion.PotionData pd = pMeta.getBasePotionData();
				//System.out.println("CP: " + pMeta.getCustomEffects());
				//String ps = pd.getType().getEffectType().getName()+":"+pd.getType().get;
				fc.set(path+".potion", pd.getType().getEffectType().getName());
	      }
      if ((s.getType().equals(Material.LEATHER_BOOTS)) || (s.getType().equals(Material.LEATHER_CHESTPLATE)) || (s.getType().equals(Material.LEATHER_HELMET)) || (s.getType().equals(Material.LEATHER_LEGGINGS))){
    	  LeatherArmorMeta l = (LeatherArmorMeta)s.getItemMeta();
    	  Color c = l.getColor();
    	  String color = c.getRed() + "," + c.getGreen() + "," + c.getBlue();
    	  fc.set(path + ".colour", color);
      }
      if ((s.getType().equals(Material.SKULL_ITEM)) && (s.getDurability() == 3)) {
    	  SkullMeta sm = (SkullMeta)s.getItemMeta();
//    	  fc.set(path + ".flags", sm.getItemFlags());
//    	  for(ItemFlag f : s)
    	  fc.set(path + ".owner", sm.getOwner());
    	  //fc.set(path + ".SkullOwner", getSkullOwner(s));
      }
      //Flags
     // System.out.println("FLAGS: " + s.getItemMeta().getItemFlags());
      if(s.getItemMeta().getItemFlags() != null){
    	  ArrayList<String> flags = new ArrayList<String>();
    	  for(ItemFlag f : s.getItemMeta().getItemFlags())
    		  if(f != null)
    			  flags.add(f.name());
    	  //System.out.println("FLAGS2: " + flags);
    	  if(!flags.isEmpty())
    		  fc.set(path + ".flags", flags);
      }
    }else{
    	System.out.println("Item is null!");
    }
    try {
      this.saveFile.save(this.saveYML);
    }catch (IOException localIOException) {}
    saveConfig();
  }
  
	/**THIS METHOD IS COPYRIGHT 2014 JACOB VEJVODA**/
	private ItemStack getItem(String path, FileConfiguration fc){
		//System.out.println("GetItem");
		try{
			//Get Item ID
			int item = fc.getInt(path + ".item");
			//Get Amount
			int setAmount = 1;
			if(fc.getString(path + ".amount") != null)
				setAmount = (fc.getInt(path + ".amount"));
			ItemStack stack = new ItemStack(item, setAmount);
			//Get Durability
			if(fc.getString(path + ".durability") != null)
				stack.setDurability((short)fc.getInt(path + ".durability"));
			//Get Name
			String name = null;
			if (fc.getString(path + ".name") != null){
				name = (fc.getString(path + ".name"));
				name = ChatColor.translateAlternateColorCodes('&', name);
			}
			//Get Lore
			ArrayList<String> loreList = new ArrayList<String>();
			for(int i = 0; i <= 10; i++) {
				if (fc.getString(path + ".lore" + i) != null){
					String lore = (fc.getString(path + ".lore" + i));
					lore = ChatColor.translateAlternateColorCodes('&', lore);	
					loreList.add(lore);
				}
			}
			//Colour
			if(fc.getString(path + ".colour") != null && stack.getType().toString().toLowerCase().contains("leather")){
				String c = fc.getString(path + ".colour");
				String[] split = c.split(",");
				Color colour = Color.fromRGB(Integer.parseInt(split[0]),Integer.parseInt(split[1]),Integer.parseInt(split[2]));
				dye(stack, colour);
			}
			//Set Name in Meta
			ItemMeta meta = stack.getItemMeta();
			if (name != null){
				meta.setDisplayName(name);
			}
			//Set Lore in Meta
			if (!loreList.isEmpty()){
				meta.setLore(loreList);
			}
			//Flags
			if(fc.getList(path + ".flags") != null){
				for(String s : (List<String>) fc.getList(path + ".flags")){
					ItemFlag f = ItemFlag.valueOf(s);
					meta.addItemFlags(f);
				}
			}
			//Set Meta to Item
			if(meta != null){
				stack.setItemMeta(meta);
			}
			//Get Enchantment Amount
			for(int e = 0; e <= 10; e++){
				if(fc.getString(path + ".enchantments." + e) != null){
					//Get Enchant
					String enchantment = (fc.getString(path + ".enchantments." + e + ".enchantment"));
					//Get Enchant Level
					int level = (fc.getInt(path + ".enchantments." + e + ".level"));
					//Add Enchant
					if(enchantment.equals("Glow")){
						EnchantGlow.addGlow(stack);
					}else if (stack.getType().equals(Material.ENCHANTED_BOOK)){
						EnchantmentStorageMeta enchantMeta = (EnchantmentStorageMeta)stack.getItemMeta();
						enchantMeta.addStoredEnchant(Enchantment.getByName(enchantment), level, true);
						stack.setItemMeta(enchantMeta);
					}else{
						stack.addUnsafeEnchantment(Enchantment.getByName(enchantment), level);
					}
				}
			}
			//Glow
			if(fc.getBoolean(path + ".glow")){
				EnchantGlow.addGlow(stack);
			}
			//Book Text
			if ((stack.getType().equals(Material.WRITTEN_BOOK)) || (stack.getType().equals(Material.BOOK_AND_QUILL))){
				BookMeta bMeta = (BookMeta)stack.getItemMeta();	
				if(fc.getString(path + ".author") != null){
					String author = fc.getString(path + ".author");
					author = ChatColor.translateAlternateColorCodes('&', author);
					bMeta.setAuthor(author);
				}
				if(fc.getString(path + ".title") != null){
					String title = fc.getString(path + ".title");
					title = ChatColor.translateAlternateColorCodes('&', title);
					bMeta.setTitle(title);
				}
				if(fc.getString(path + ".pages") != null)
					for(String i : fc.getConfigurationSection(path + ".pages").getKeys(false)){
						String page = fc.getString(path + ".pages." + i);
						page = ChatColor.translateAlternateColorCodes('&', page);
						bMeta.addPage(page);
  				}
				stack.setItemMeta(bMeta);
			}
			//Banners
			if(stack.getType().equals(Material.BANNER)){
				BannerMeta b = (BannerMeta) stack.getItemMeta();
				List<Pattern> patList = (List<Pattern>) fc.getList(path + ".patterns");
				if(patList != null && (!patList.isEmpty()))
					b.setPatterns(patList);
				stack.setItemMeta(b);
			}
		    //Shield
			if(is9())
				if(stack.getType().equals(Material.SHIELD)){
			    	   ItemMeta im = stack.getItemMeta();
			           BlockStateMeta bmeta = (BlockStateMeta) im;
	
			           Banner b = (Banner) bmeta.getBlockState();
			           List<Pattern> patList = (List<Pattern>) fc.getList(path + ".patterns");
			           b.setBaseColor(DyeColor.valueOf(fc.getString(path + ".colour")));
			           b.setPatterns(patList);
			           b.update();
			           bmeta.setBlockState(b);
			           stack.setItemMeta(bmeta);
				}
//		      if (stack.getType().equals(Material.SHIELD)){
//		    	   ItemMeta im = stack.getItemMeta();
//		           BlockStateMeta bmeta = (BlockStateMeta) im;
//
//		           Banner banner = (Banner) bmeta.getBlockState();
//		           //banner.setBaseColor(DyeColor.BLUE);
//		           //banner.addPattern(new Pattern(DyeColor.WHITE, PatternType.CREEPER));
//		           List<Pattern> patList = (List<Pattern>) fc.getList(path + ".patterns");
//		           if(patList != null && (!patList.isEmpty()))
//		        	   banner.setPatterns(patList);
//		           //banner.update();
//
//		           bmeta.setBlockState(banner);
//		           stack.setItemMeta(bmeta);
//		      }
			//Potions
			if(is9())
				if(fc.getString(path + ".potion") != null)
					if(stack.getType().equals(Material.POTION) || stack.getType().equals(Material.SPLASH_POTION) || stack.getType().equals(Material.LINGERING_POTION)){
						PotionMeta pMeta = (PotionMeta) stack.getItemMeta();
						String pn = fc.getString(path+".potion");
						pMeta.setBasePotionData(new org.bukkit.potion.PotionData(PotionType.getByEffect(PotionEffectType.getByName(pn)), fc.getBoolean(path+".potionExtended"), fc.getBoolean(path+".potionUpgraded")));
	
						stack.setItemMeta(pMeta);
					}
			//Owner
			//System.out.println("T: " + stack.getType());
			//System.out.println("D: " + stack.getDurability());
			if(stack.getType().equals(Material.SKULL_ITEM) && (stack.getDurability() == 3)){
				//String[] split = stack.getItemMeta().getDisplayName().split(" ");
				//System.out.println("O: " + split[2]);
				//fc.set(path + ".owner", split[2]);
				String owner = fc.getString(path + ".owner");
				SkullMeta sm = (SkullMeta) stack.getItemMeta();
				sm.setOwner(owner);
				stack.setItemMeta(sm);
				//stack = setSkullOwner(stack, fc, path + ".SkullOwner");
			}
			return stack;
		}catch(Exception e){this.getLogger().log(Level.SEVERE, e.getMessage(), true); e.printStackTrace();}
		//System.out.println("Error in item builder: No valid items found!");
		return null;
	}
  
//    private String getSkullOwner(ItemStack itemStack){
//    	CraftItemStack craftItemStack = CraftItemStack.asCraftCopy(itemStack);
//    	if (craftItemStack.getType() != Material.AIR) { // AIR cannot contain NBT
//    	    net.minecraft.server.v1_9_R1.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(craftItemStack);
//    	    NBTTagCompound compound = nmsItemStack.getTag();
//
//    	    if (compound == null)
//    	        nmsItemStack.setTag(compound = new NBTTagCompound()); // Just incase it was null
//    	    
//    	    return compound.get("SkullOwner").toString();
//    	}
//    	return null;
//    }
//	
//    private ItemStack setSkullOwner(ItemStack itemStack, FileConfiguration fc, String p){
//    	CraftItemStack craftItemStack = CraftItemStack.asCraftCopy(itemStack);
//    	if (craftItemStack.getType() != Material.AIR) { // AIR cannot contain NBT
//    	    net.minecraft.server.v1_9_R1.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(craftItemStack);
//    	    NBTTagCompound compound = nmsItemStack.getTag();
//
//    	    if (compound == null)
//    	        nmsItemStack.setTag(compound = new NBTTagCompound()); // Just incase it was null
//    	    
//    	    try {
//				compound.set("SkullOwner", MojangsonParser.parse(fc.getString(p)));
//			} catch (MojangsonParseException e) {
//				e.printStackTrace();
//			}
//
//    	    nmsItemStack.setTag(compound);
//    	    itemStack = CraftItemStack.asBukkitCopy(nmsItemStack); // All done
//    	    return itemStack;
//    	}
//    	return null;
//    }
	
  public void dye(ItemStack item, Color color)
  {
    try
    {
      LeatherArmorMeta meta = (LeatherArmorMeta)item.getItemMeta();
      meta.setColor(color);
      item.setItemMeta(meta);
    }
    catch (Exception localException) {}
  }
  
  public Block blockNear(Location l, Material mat, int radius)
  {
    double xTmp = l.getX();
    double yTmp = l.getY();
    double zTmp = l.getZ();
    int finalX = (int)Math.round(xTmp);
    int finalY = (int)Math.round(yTmp);
    int finalZ = (int)Math.round(zTmp);
    for (int x = finalX - radius; x <= finalX + radius; x++) {
      for (int y = finalY - radius; y <= finalY + radius; y++) {
        for (int z = finalZ - radius; z <= finalZ + radius; z++)
        {
          Location loc = new Location(l.getWorld(), x, y, z);
          Block block = loc.getBlock();
          if (block.getType().equals(mat)) {
            return block;
          }
        }
      }
    }
    return null;
  }
  
  private String getLocationName(Location l)
  {
    return (l.getX() + "." + l.getY() + "." + l.getZ() + l.getWorld().getName()).replace(".", "");
  }
  
  private void showMana(Player p)
  {
    int max = getConfig().getInt("startingMana");
    if (this.saveFile.getString("players." + p.getUniqueId().toString() + ".mana") != null) {
      max = this.saveFile.getInt("players." + p.getUniqueId().toString() + ".mana");
    }
    int mana = max;
    if (this.playerMana.containsKey(p.getName())) {
      mana = ((Integer)this.playerMana.get(p.getName())).intValue();
    }
    p.sendMessage("§4Mana: §d" + mana + "/" + max);
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onPlayerClick(InventoryClickEvent e)
  {
    String name = e.getInventory().getName();
    Player p = (Player)e.getWhoClicked();
    try
    {
      String n = e.getCurrentItem().getItemMeta().getDisplayName();
      if (name.contains("§0§lSet Spell Recipe"))
      {
        if (n.equals("§f")) {
          e.setCancelled(true);
        } else if (n.equals("§eDone")) {
          saveRecipe("spells.<id>.recipe", p);
        }
      }
      else if (name.contains("§0§lSet Staff Recipe"))
      {
        if (n.equals("§f")) {
          e.setCancelled(true);
        } else if (n.equals("§eDone")) {
          saveRecipe("staffs.<id>.recipe", p);
        }
      }
      else if (name.contains("§0§lSet Cap Recipe"))
      {
        if (n.equals("§f")) {
          e.setCancelled(true);
        } else if (n.equals("§eDone")) {
          saveRecipe("staffCaps.<id>.recipe", p);
        }
      }
      else if (name.contains("§0§lSpell Book"))
      {
        if (!n.contains(" §5Spell")) {
          e.setCancelled(true);
        }
      }
      else if (name.contains("§0§lBind Staff"))
      {
        if (!n.contains(" §5Spell")) {
          e.setCancelled(true);
        }
      }
      else if (name.contains(" §0§lRecipe")) {
        e.setCancelled(true);
      }
    }
    catch (Exception localException) {}
  }
  
  private void saveRecipe(String path, Player p)
  {
    String id = (String)this.recipeMap.get(p.getName());
    Inventory inv = (Inventory)this.invMap.get(p.getName());
    getConfig().set(path.replace("<id>", id), null);
    List<Integer> ids = new ArrayList(Arrays.asList(new Integer[] { Integer.valueOf(0), Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(9), Integer.valueOf(10), Integer.valueOf(11), Integer.valueOf(18), Integer.valueOf(19), Integer.valueOf(20) }));
    for (int i = 0; i < 9; i++) {
      if ((inv.getItem(((Integer)ids.get(i)).intValue()) != null) && (!inv.getItem(((Integer)ids.get(i)).intValue()).getType().equals(Material.AIR))) {
        setItem(inv.getItem(((Integer)ids.get(i)).intValue()), path.replace("<id>", id) + "." + i, getConfig());
      } else {
        getConfig().set(path.replace("<id>", id) + "." + i, null);
      }
    }
    saveConfig();
    p.closeInventory();
    p.sendMessage("§eRecipe saved!");
    this.recipeMap.remove(p.getName());
    this.invMap.remove(p.getName());
  }
  
  public void openRecipeMakeMenu(Player p, String id, String type)
  {
    this.recipeMap.put(p.getName(), id);
    Inventory inv;
    if (type.equals("staffs"))
    {
      inv = getServer().createInventory(p, 27, "§0§lSet Staff Recipe");
    }
    else
    {
      if (type.equals("staffCaps")) {
        inv = getServer().createInventory(p, 27, "§0§lSet Cap Recipe");
      } else {
        inv = getServer().createInventory(p, 27, "§0§lSet Spell Recipe");
      }
    }
    ArrayList<Integer> ids = new ArrayList(Arrays.asList(new Integer[] { Integer.valueOf(3), Integer.valueOf(4), Integer.valueOf(5), Integer.valueOf(6), Integer.valueOf(7), Integer.valueOf(8), Integer.valueOf(12), Integer.valueOf(14), Integer.valueOf(15), Integer.valueOf(17), Integer.valueOf(21), Integer.valueOf(22), Integer.valueOf(23), Integer.valueOf(24), Integer.valueOf(25), Integer.valueOf(26) }));
    for (Iterator localIterator = ids.iterator(); localIterator.hasNext();)
    {
      int i = ((Integer)localIterator.next()).intValue();
      ItemStack n = getItem("160", "§f", 1, null);
      inv.setItem(i, n);
    }
    List<Integer> rids = new ArrayList(Arrays.asList(new Integer[] { Integer.valueOf(0), Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(9), Integer.valueOf(10), Integer.valueOf(11), Integer.valueOf(18), Integer.valueOf(19), Integer.valueOf(20) }));
    String path;
    if (type.equals("staffs"))
    {
      path = "staffs." + id + ".recipe.";
    }
    else
    {
      if (type.equals("staffCaps")) {
        path = "staffCaps." + id + ".recipe.";
      } else {
        path = "spells." + id + ".recipe.";
      }
    }
    for (int i = 0; i < 9; i++) {
      if (getConfig().getString(path + i) != null) {
        inv.setItem(((Integer)rids.get(i)).intValue(), getItem(path + i, getConfig()));
      }
    }
    ItemStack n = getItem("160:4", "§eDone", 1, null);
    inv.setItem(16, n);
    if (type.equals("staffs")) {
      inv.setItem(13, getStaff(id));
    } else if (type.equals("staffCaps")) {
      inv.setItem(13, getItem("staffCaps." + id, getConfig()));
    } else {
      inv.setItem(13, getSpell(id));
    }
    p.openInventory(inv);
    this.invMap.put(p.getName(), inv);
  }
  
	 private void setLocation(String path, Location l, FileConfiguration fc){
		 fc.set(path + ".world", l.getWorld().getName());
		 fc.set(path + ".x", l.getX());
		 fc.set(path + ".y", l.getY());
		 fc.set(path + ".z", l.getZ());
		 fc.set(path + ".pitch", l.getPitch());
		 fc.set(path + ".yaw", l.getYaw());
		 saveConfig();
		 try{
			 this.saveFile.save(this.saveYML);
		 }catch (IOException localIOException) {}
	 }
  
	 private Location getLocation(String path, FileConfiguration fc){
		 World world = getServer().getWorld(fc.getString(path + ".world"));
		 double x = fc.getDouble(path + ".x");
		 double y = fc.getDouble(path + ".y");
		 double z = fc.getDouble(path + ".z");
		 float pitch = (float) fc.getDouble(path + ".pitch");
		 float yaw = (float) fc.getDouble(path + ".yaw");
		 Location loc = new Location(world, x, y, z);
		 loc.setPitch(pitch);
		 loc.setYaw(yaw);
		 return loc;
	 }
  
  public void openRecipeDisplayMenu(Player p, String spell){
    Inventory inv = getServer().createInventory(p, InventoryType.DISPENSER, "§0§l" + spell + " §0§lRecipe");
    for (int i = 0; i < 9; i++) {
      if (getConfig().getString("spells." + spell + ".recipe." + i) != null) {
    	//System.out.println("path: " + "spells." + spell + ".recipe." + i);
    	ItemStack s = getItem("spells." + spell + ".recipe." + i, getConfig());
        inv.setItem(i, s);
      }
    }
    p.openInventory(inv);
  }
  
  public int rand(int min, int max){
    int r = min + (int)(Math.random() * (1 + max - min));
    return r;
  }
  
  public double rand(double mind, double maxd){
    int min = (int)(mind * 10.0D);
    int max = (int)(maxd * 10.0D);
    int r = rand(min, max);
    double rd = r / 10.0D;
    return rd;
  }
  
  public ItemStack getStaffWithSpells(int id, ArrayList<String> spells){
    ItemStack staff = getStaff(id+"");
    String[] s = ((String)staff.getItemMeta().getLore().get(0)).replace("§k", "").split(":");
    int uuid = Integer.parseInt(s[0]);
    if ((spells != null) && (!spells.isEmpty()))
    {
      for (int i = 0; i < spells.size(); i++) {
        this.saveFile.set("staffs." + uuid + "." + i, spells.get(i));
      }
      try
      {
        this.saveFile.save(this.saveYML);
      }
      catch (IOException localIOException) {}
    }
    return staff;
  }
  
  private String getPotionName(int dur){
    switch (dur)
    {
    case 16: 
      return "Awkward Potion";
    case 32: 
      return "Thick Potion";
    case 64: 
      return "Mundane Potion";
    case 8193: 
      return "Regeneration Potion";
    case 8194: 
      return "Swiftness Potion";
    case 8195: 
      return "Fire Resistance Potion";
    case 8196: 
      return "Poison Potion";
    case 8197: 
      return "Healing Potion";
    case 8198: 
      return "Night Vision Potion";
    case 8200: 
      return "Weakness Potion";
    case 8201: 
      return "Strength Potion";
    case 8202: 
      return "Slowness Potion";
    case 8204: 
      return "Harming Potion";
    case 8205: 
      return "Water Breathing Potion";
    case 8206: 
      return "Invisibility Potion";
    case 8225: 
      return "Regeneration Potion II";
    case 8226: 
      return "Swiftness Potion II";
    case 8228: 
      return "Poison Potion II";
    case 8229: 
      return "Healing Potion II";
    case 8233: 
      return "Strength Potion II";
    case 8235: 
      return "Leaping Potion II";
    case 8236: 
      return "Harming Potion II";
    case 8257: 
      return "Regeneration Potion";
    case 8258: 
      return "Swiftness Potion";
    case 8259: 
      return "Fire Resistance Potion";
    case 8260: 
      return "Poison Potion";
    case 8262: 
      return "Night Vision Potion";
    case 8264: 
      return "Weakness Potion";
    case 8265: 
      return "Strength Potion";
    case 8266: 
      return "Slowness Potion";
    case 8267: 
      return "Leaping Potion";
    case 8269: 
      return "Water Breathing Potion";
    case 8270: 
      return "Invisibility Potion";
    case 8289: 
      return "Regeneration Potion II";
    case 8290: 
      return "Swiftness Potion II";
    case 8292: 
      return "Poison Potion II";
    case 8297: 
      return "Strength Potion II";
    case 16389: 
      return "Healing Splash";
    }
    return "Water Bottle";
  }
  
  private static List<Chunk> getNearbyChunks(Location l, int range)
  {
    List<Chunk> chunkList = new ArrayList();
    World world = l.getWorld();
    int chunks = range / 16 + 1;
    for (int x = l.getChunk().getX() - chunks; x < l.getChunk().getX() + chunks; x++) {
      for (int z = l.getChunk().getZ() - chunks; z < l.getChunk().getZ() + chunks; z++)
      {
        Chunk chunk = world.getChunkAt(x, z);
        if ((chunk != null) && (chunk.isLoaded())) {
          chunkList.add(chunk);
        }
      }
    }
    return chunkList;
  }
  
  private static List<Entity> getEntitiesInNearbyChunks(Location l, int range, List<EntityType> entityTypes)
  {
    List<Entity> entities = new ArrayList();
    for (Chunk chunk : getNearbyChunks(l, range)) {
      if (entityTypes == null)
      {
        entities.addAll(Arrays.asList(chunk.getEntities()));
      }
      else
      {
        Entity[] arrayOfEntity;
        int j = (arrayOfEntity = chunk.getEntities()).length;
        for (int i = 0; i < j; i++)
        {
          Entity e = arrayOfEntity[i];
          if (entityTypes.contains(e.getType())) {
            entities.add(e);
          }
        }
      }
    }
    return entities;
  }
  
  private static List<Entity> getNearbyEntities(Location l, float range, List<EntityType> entityTypes){
	  List<Entity> entities = new ArrayList();
	  for (Entity e : getEntitiesInNearbyChunks(l, (int)range, entityTypes)) {
		  if(e.getLocation().getWorld().getName().equals(l.getWorld().getName()))
			  if (e.getLocation().distance(l) <= range) {
				  entities.add(e);
			  }
	  }
	  return entities;
  }
  
  public boolean canAttack(Player p, Location l){
    try{
      if (getConfig().getBoolean("enablePVPSpells")) {
        return true;
      }
      //boolean attack = true;
      if (Bukkit.getServer().getPluginManager().getPlugin("WorldGuard") != null)
    	  return new WorldGuardMethods().canAttack(p, l);
    }catch (Exception e){
      e.printStackTrace();
    }
    return true;
  }
  
  public boolean canBuild(Player p, Location l){
    try{
      boolean build = false;
      int pl = 0;
      factions:
	      if (Bukkit.getServer().getPluginManager().getPlugin("Factions") != null) {
	        try{
	          build = true;
	          MPlayer mplayer = MPlayer.get(p);
	          Faction faction = BoardColl.get().getFactionAt(PS.valueOf(l));
	          if ((faction == null) || (mplayer.getFaction().equals(faction))) {
	            break factions;
	          }
	          build = false;
	          if(getConfig().getBoolean("debug"))
	        	  this.getLogger().log(Level.INFO, "Factions says " + p.getName() + " can not build!");
	        }catch (Exception localException1) {}
	      }else
	        pl++;
      
      if (Bukkit.getServer().getPluginManager().getPlugin("WorldGuard") != null) {
        build = new WorldGuardMethods().canBuild(p, l);
      }else
        pl++;
      towny:
	      if (Bukkit.getServer().getPluginManager().getPlugin("Towny") != null) {
	        try{
	          boolean bBuild = PlayerCacheUtil.getCachePermission(p, l, Integer.valueOf(l.getBlock().getTypeId()), l.getBlock().getData(), ActionType.BUILD);
	          if (!bBuild) {
		          if(getConfig().getBoolean("debug"))
		        	  this.getLogger().log(Level.INFO, "Towny says " + p.getName() + " can not build!");
	            break towny;
	          }
	          build = true;
	        }catch (Exception localException2) {}
	      }else
	        pl++;
      if (pl == 3)
          return true;
      return build;
    }catch (Exception e){e.printStackTrace();}
    return false;
  }
  
  private boolean is9(){
	  if(Bukkit.getVersion().contains("1.9") || Bukkit.getVersion().contains("1.10") || Bukkit.getVersion().contains("1.11") || Bukkit.getVersion().contains("1.12"))
		  return true;
	  return false;
  }
  
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
  {
    try
    {
      if ((cmd.getName().equalsIgnoreCase("wizardlymagic")) || (cmd.getName().equals("wm")))
      {
        if (args[0].equalsIgnoreCase("reload"))
        {
          reloadConfig();
          addRecipes();
          sender.sendMessage("§4WMagic: §dReloaded config!");
          return true;
        }
        if ((args[0].equalsIgnoreCase("getSpell")) && (args.length == 2))
        {
          if ((sender instanceof Player))
          {
            Player p = (Player)sender;
            if (getConfig().getString("spells." + args[1] + ".mana") != null)
            {
              p.getInventory().addItem(new ItemStack[] { getSpell(args[1]) });
              sender.sendMessage("§4WMagic: §dSpell given!");
            }
            else
            {
              sender.sendMessage("§4WMagic: §dThere is no spell called " + args[1] + "!");
            }
          }
          else
          {
            sender.sendMessage("§4WMagic: §dThis command can only be run by a player!");
          }
          return true;
        }
        if ((args[0].equalsIgnoreCase("setSpellRecipe")) && (args.length == 2))
        {
          if ((sender instanceof Player))
          {
            Player p = (Player)sender;
            if (getConfig().getString("spells." + args[1]) != null) {
              openRecipeMakeMenu(p, args[1], "spell");
            } else {
              sender.sendMessage("§4WMagic: §dNo spell with that name!");
            }
          }
          else
          {
            sender.sendMessage("§4WMagic: §dThis command can only be run by a player!");
          }
          return true;
        }
        if (args[0].equalsIgnoreCase("spellBook"))
        {
          if ((sender instanceof Player))
          {
            Player p = (Player)sender;
            p.getInventory().addItem(new ItemStack[] { getSpellBook() });
            sender.sendMessage("§4WMagic: §dSpell book given!");
          }
          else
          {
            sender.sendMessage("§4WMagic: §dThis command can only be run by a player!");
          }
          return true;
        }
        if ((args[0].equalsIgnoreCase("getDrops")) && (args.length == 2))
        {
          if ((sender instanceof Player))
          {
            Player p = (Player)sender;
            if (getConfig().getString("mobDrops." + args[1]) != null)
            {
              for (String i : getConfig().getConfigurationSection("mobDrops." + args[1]).getKeys(false)) {
                p.getInventory().addItem(new ItemStack[] { getItem("mobDrops." + args[1] + "." + i, getConfig()) });
              }
              sender.sendMessage("§4WMagic: §dMob's special drops given.");
            }
            else
            {
              sender.sendMessage("§4WMagic: §dNo mob with that name!");
            }
          }
          else
          {
            sender.sendMessage("§4WMagic: §dThis command can only be run by a player!");
          }
          return true;
        }if ((args[0].equalsIgnoreCase("giveSpell")) && (args.length == 3)){
          if (getServer().getPlayer(args[2]) != null){
            Player p = getServer().getPlayer(args[2]);
            if (getConfig().getString("spells." + args[1] + ".mana") != null){
              p.getInventory().addItem(new ItemStack[] { getSpell(args[1]) });
              sender.sendMessage("§4WMagic: §dSpell given!");
            }else{
              sender.sendMessage("§4WMagic: §dThere is no spell called " + args[1] + "!");
            }
          }else{
            sender.sendMessage("§4WMagic: §dPlayer not found!");
          }
          return true;
        }if (args[0].equalsIgnoreCase("listSpells")){
        	  sender.sendMessage("§4WMagic Spells:");
        	  for(String spell : getConfig().getConfigurationSection("spells").getKeys(false))
        		  sender.sendMessage("§d"+spell);
              return true;
        }if ((args[0].equalsIgnoreCase("setStaffRecipe")) && (args.length == 2)){
          if ((sender instanceof Player)){
            Player p = (Player)sender;
            if (getConfig().getString("staffs." + args[1]) != null) {
              openRecipeMakeMenu(p, args[1], "staffs");
            } else {
              sender.sendMessage("§4WMagic: §dNo staff with that id!");
            }
          }else{
            sender.sendMessage("§4WMagic: §dThis command can only be run by a player!");
          }
          return true;
        }
        if ((args[0].equalsIgnoreCase("setCapRecipe")) && (args.length == 2))
        {
          if ((sender instanceof Player))
          {
            Player p = (Player)sender;
            if (getConfig().getString("staffCaps." + args[1]) != null) {
              openRecipeMakeMenu(p, args[1], "staffCaps");
            } else {
              sender.sendMessage("§4WMagic: §dNo staff cap with that id!");
            }
          }
          else
          {
            sender.sendMessage("§4WMagic: §dThis command can only be run by a player!");
          }
          return true;
        }
        if ((args[0].equalsIgnoreCase("staff")) && (args.length >= 3))
        {
          if (getServer().getPlayer(args[1]) != null)
          {
            Player p = getServer().getPlayer(args[1]);
            ArrayList<String> spells = new ArrayList();
            for (int i = 3; i < args.length; i++) {
              try
              {
                spells.add(args[i]);
              }
              catch (Exception localException) {}
            }
            p.getInventory().addItem(new ItemStack[] { getStaffWithSpells(Integer.parseInt(args[2]), spells) });
            sender.sendMessage("§4WMagic: §dStaff book given!");
          }
          else
          {
            sender.sendMessage("§4WMagic: §dThis command can only be run by a player!");
          }
          return true;
        }
        if ((args[0].equalsIgnoreCase("mana")) && (args.length >= 3))
        {
          Player p = getServer().getPlayer(args[2]);
          if (p != null)
          {
            if (args[1].equalsIgnoreCase("setmax"))
            {
              setMana(p, Integer.parseInt(args[3]));
              sender.sendMessage("§4WMagic: §d" + p.getName() + "'s §r§dmana has been set to " + args[3] + "!");
            }
            else if (args[1].equalsIgnoreCase("add"))
            {
              int pMana = getPlayerMana(p);
              this.playerMana.put(p.getName(), Integer.valueOf(pMana + Integer.parseInt(args[3])));
              sender.sendMessage("§4WMagic: §d" + p.getName() + " §r§dhas been given " + args[3] + " mana!");
            }
            else if (args[1].equalsIgnoreCase("remove"))
            {
              int pMana = getPlayerMana(p);
              this.playerMana.put(p.getName(), Integer.valueOf(pMana - Integer.parseInt(args[3])));
              sender.sendMessage("§4WMagic: §d" + p.getName() + " §r§dhas had " + args[3] + " mana taken from them!");
            }
            else if (args[1].equalsIgnoreCase("reset"))
            {
              setMana(p, getConfig().getInt("startingMana"));
              sender.sendMessage("§4WMagic: §d" + p.getName() + " §r§dhas had their mana reset!");
            }
            else
            {
              sender.sendMessage("§4WMagic: §dInvalid command!");
            }
          }
          else {
            sender.sendMessage("§4WMagic: §dPlayer not online!");
          }
          return true;
        }
        if ((args[0].equalsIgnoreCase("knowledge")) && (args.length >= 3))
        {
          Player p = getServer().getPlayer(args[2]);
          if (p != null)
          {
            ArrayList<String> ak = getAllKnowledge();
            try
            {
              if (!ak.contains(args[3]))
              {
                sender.sendMessage("§4WMagic: §dThat knowledge does not exist!");
                return true;
              }
            }
            catch (Exception localException4)
            {
              if (args[1].equalsIgnoreCase("add"))
              {
                checkKnowledge(p, getKnowledgeID(args[3]), getKnowledgeType(args[3]), false);
                sender.sendMessage("§4WMagic: §d" + args[3] + "§r§d has been added to " + p.getName() + "§r§d!");
              }
              else if (args[1].equalsIgnoreCase("remove"))
              {
                Object pk = getKnowledge(p);
                ((ArrayList)pk).remove(args[3]);
                this.saveFile.set("players." + p.getUniqueId().toString() + ".knowledge", pk);
                try
                {
                  this.saveFile.save(this.saveYML);
                }
                catch (IOException localIOException) {}
                sender.sendMessage("§4WMagic: §d" + args[3] + "§r§d has been removed from " + p.getName() + "§r§d!");
              }
              else if (args[1].equalsIgnoreCase("addAll"))
              {
                this.saveFile.set("players." + p.getUniqueId().toString() + ".knowledge", ak);
                try
                {
                  this.saveFile.save(this.saveYML);
                }
                catch (IOException localIOException3) {}
                sender.sendMessage("§4WMagic: §dAll knowledge has been added to " + p.getName() + "§r§d!");
              }
              else if (args[1].equalsIgnoreCase("removeAll"))
              {
                this.saveFile.set("players." + p.getUniqueId().toString() + ".knowledge", null);
                try
                {
                  this.saveFile.save(this.saveYML);
                }
                catch (IOException localIOException4) {}
                sender.sendMessage("§4WMagic: §dAll knowledge has been removed from " + p.getName() + "§r§d!");
              }
              else
              {
                sender.sendMessage("§4WMagic: §dInvalid command!");
              }
            }
          }
          sender.sendMessage("§4WMagic: §dPlayer not online!");
          return true;
        }
      } else{
    	  if (cmd.getName().equalsIgnoreCase("mana")){
    		  if ((sender instanceof Player)) {
    			  Player p = (Player)sender;
    			  try{
    				  if (args[0].equalsIgnoreCase("on")){
    					  this.showMana.put(p.getName(), Boolean.valueOf(true));
    					  sender.sendMessage("§4WMagic: §dMana use will be displayed!");
    					  return true;
    				  }
    				  if (args[0].equalsIgnoreCase("off")){
    					  this.showMana.put(p.getName(), Boolean.valueOf(false));
    					  sender.sendMessage("§4WMagic: §dMana use will be hidden!");
    					  return true;
    				  }
    			  }catch (Exception localException2){showMana(p);}
    		  }else
    			  sender.sendMessage("§4WMagic: §dThis command can only be run by a player!");
    		  return true;
    	  }
    	  if ((cmd.getName().equalsIgnoreCase("spellrecipe")) || (cmd.getName().equalsIgnoreCase("spellr")) || (cmd.getName().equalsIgnoreCase("srecipe")) || (cmd.getName().equalsIgnoreCase("sr"))) {
    		  if ((sender instanceof Player)){
    			  Player p = (Player)sender;
    			  String spell = null;
    			  for (String s : getConfig().getConfigurationSection("spells").getKeys(false)) {
    				  if (s.equalsIgnoreCase(args[0])){
    					  spell = s;
    					  break;
    				  }
    			  }
    			  if (spell != null){
    				  if (getConfig().getString("spells." + spell + ".recipe") != null) {
    					  openRecipeDisplayMenu(p, spell);
    				  }else
    					  sender.sendMessage("§4WMagic: §dThe spell " + spell + " is not craft-able!");
    			  }else
    				  sender.sendMessage("§4WMagic: §dNo spell with that name!");
    		  }else
    			  sender.sendMessage("§4WMagic: §dThis command can only be run by a player!");
    		  return true;
    	  }
      }
    }catch (Exception x){/**x.printStackTrace();**/}
      sender.sendMessage("§4--§5WizardlyMagic v" + Bukkit.getServer().getPluginManager().getPlugin("WizardlyMagic").getDescription().getVersion() + "§4--");
      if (sender.hasPermission("wizardly.magic.commands")){
        sender.sendMessage("§d/wm reload");
        sender.sendMessage("§d/wm getSpell <spell>");
        sender.sendMessage("§d/wm giveSpell <spell> <player>");
        sender.sendMessage("§d/wm listSpells");
        sender.sendMessage("§d/wm setSpellRecipe <spell>");
        sender.sendMessage("§d/wm setStaffRecipe <id>");
        sender.sendMessage("§d/wm setCapRecipe <id>");
        sender.sendMessage("§d/wm spellBook");
        sender.sendMessage("§d/wm getDrops <mob>");
        sender.sendMessage("§d/wm staff <player> <staff ID> <spell, spell...>");
        sender.sendMessage("§d/wm mana <setmax/add/remove/reset> <player> <amount>");
        sender.sendMessage("§d/wm knowledge <addAll/add/remove/removeAll> <player> <knowledge>");
      }
      sender.sendMessage("§d/mana <on/of>");
      sender.sendMessage("§d/srecipe <spell>");
    return true;
  }
  
  public class imgMapRenderer extends MapRenderer{
    String imgName = "default.png";
    
    public imgMapRenderer() {}
    
    public void render(MapView mapView, MapCanvas mapCanvas, Player player){
      try{
        File plugins = Bukkit.getServer().getPluginManager().getPlugin("WizardlyMagic").getDataFolder().getParentFile();
        
        File folder = new File(plugins.getPath() + File.separator + "WizardlyMagic" + File.separator + "spell.png");
        BufferedImage img = ImageIO.read(folder);
        mapView.setScale(MapView.Scale.NORMAL);
        mapCanvas.drawImage(5, 5, img);
      }catch (IOException e){e.printStackTrace();}
    }
    
    public void setImage(String s)
    {
      this.imgName = s;
    }
  }
  
  class WorldGuardMethods{
    WorldGuardMethods() {}
    
    private  WorldGuardPlugin getWorldGuard(){
      Plugin plugin = WizardlyMagic.this.getServer().getPluginManager().getPlugin("WorldGuard");
      if ((plugin == null) || (!(plugin instanceof WorldGuardPlugin))) {
        return null;
      }
      return (WorldGuardPlugin)plugin;
    }
    
    public boolean canAttack(Player p, Location l)
    {
      boolean attack = true;
      try
      {
        WorldGuardPlugin wg = getWorldGuard();
        RegionManager regionManager = wg.getRegionManager(l.getWorld());
        ApplicableRegionSet set = regionManager.getApplicableRegions(l);
        try
        {
          ProtectedRegion r = regionManager.getRegion("__global__");
          State s = (State)r.getFlag(DefaultFlag.PVP);
          if (s.toString().equals("DENY")) {
            attack = false;
          }
        }
        catch (Exception localException1) {}
        if (!set.getRegions().isEmpty()) {
          if (set.allows(DefaultFlag.PVP)) {
            attack = true;
          } else {
            attack = false;
          }
        }
      }
      catch (Exception x)
      {
        p.sendMessage(x.getMessage());x.printStackTrace();
      }
      return attack;
    }
    
    public boolean canBuild(Player p, Location l) {
    	boolean build = false;
    	try{
//    		if (getWorldGuard().canBuild(p, l))
//    			build = true;
//    		if (!build){
    			WorldGuardPlugin wg = getWorldGuard();
    			RegionManager regionManager = wg.getRegionManager(l.getWorld());
    			ApplicableRegionSet set = regionManager.getApplicableRegions(l);

    			ProtectedRegion r = regionManager.getRegion("__global__");
    			State s = (State)r.getFlag(DefaultFlag.BUILD);
    			if (s.toString().equals("DENY")) {
    				build = false;
    				if(getConfig().getBoolean("debug"))
    					WizardlyMagic.this.getLogger().log(Level.INFO, "WorldGuard Global says " + p.getName() + " can not build!");
    			}else{
    				build = true;
    			}
    			if (!set.getRegions().isEmpty()) {
    				if (set.allows(DefaultFlag.BLOCK_PLACE)) {
    					build = true;
    				}else{
    					if(getConfig().getBoolean("debug"))
    						WizardlyMagic.this.getLogger().log(Level.INFO, "WorldGuard Region says " + p.getName() + " can not build!");
    					return false;
    				}
    			}
    		//}
    	}catch (Exception localException) {}
    	return build;
    }
  }
}
